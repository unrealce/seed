var TIMEOUT = 3000;

/**
 *  日志
 *  trace 跟踪
 *  debug 调试
 *  info  信息
 *  warn  警告
 *  error 错误
 *  fatal 致命
 */
var log4js = require('log4js'); // Port of Log4js to work with node
var logger = log4js.getLogger('img');
logger.setLevel('INFO');
var pmx = require('pmx'); // Keymetrics++ and PM2 adapter

/**
 *  Fetch url contents.
 *  Supports gzipped content for quicker download, redirects (with automatic cookie handling, so no eternal redirect loops), streaming and piping etc.
 */
var fetchUrl = require('fetch').fetchUrl;
var gm = require('gm').subClass({ imageMagick: true }); // GraphicsMagick and ImageMagick for node.js

var oss = require('../oss.js'); // 阿里云 OSS

var storeSizes = { // 设备尺寸
  android: { width: 280 },
  iPhone4: { width: 640 },
  iPhone6: { width: 750 },
  iPhone6p: { width: 1242 },
  weixin: { width: 64, height: 64 }
};

/**
 *  标题图片
 */
var uploadImgs = function (image, encode) {
  if (!image || !image.url) {
    return;
  }
  var id = image._id;
  var url = image.url;
  var type = image.imgType;
  fetchUrl(url, function (err, res, buf) {
    var errMsg; // 错误信息
    if (err) {
      errMsg = 'fetch url ' + id + '[' +  url + '] error: ' + err;
    } else if (res.status != 200) {
      errMsg = 'fetch url ' + id + '[' +  url + '] statusCode: ' + res.status;
    } else if (!buf) {
      errMsg = 'fetch url ' + id + '[' +  url + '] buf empty';
    }
    if (errMsg) { // 遇到以上错误
      pmx.notify({ event: 'fetchUrl', error: errMsg });
      logger.error(errMsg);
      if (!encode) {
        image.url = encodeURI(url);
        uploadImgs(image, true);
      }
      return;
    }
    logger.info('%s image:', id, url);
    logger.debug('%s image buf:', id, buf);
    var key = 'images/' + id;
    for (var store in storeSizes) {
      var keyStore = key + '.' + store;
      var storeSize = storeSizes[store];
      logger.trace('%s image key:', id, keyStore);
      logger.trace('%s %s\'s size:', id, store, storeSize);
      uploadImg(buf, keyStore, type, storeSize.width, storeSize.height);
    }
  });
};

/**
 *  上传图片
 */
var uploadImg = function (body, key, type, width, height) {
  /**
   *  gm 处理
   */
  var file = gm(body);
  file.flatten(); // flatten a sequence of images
  file.resize(width, height); // 缩放
  file.quality(40); // 质量压缩
  file.toBuffer('JPEG', function (err, buffer) {
    var contentType = 'image/jpeg';
    if (err) {
      pmx.notify({ event: 'gm', error: key + ' gm to buffer error:' + err });
      logger.error('%s gm to buffer error:', key, err);
      /**
       *  gm to buffer 失败传原图
       */
      buffer = body; // 上传原图到阿里云 OSS
    } else {
      type = 'image/jpeg';
    }
    logger.trace('upload img:', key);
    oss.upload(type, buffer, key, function (err, data) { // 上传缩略图到阿里云 OSS
      if (err) {
        pmx.notify({ event: 'oss put object', error: 'upload [' + key + '] error:' + err });
        logger.error('upload [%s] error:', key, err);
        return;
      }
      logger.trace('upload [%s] success:', key, data);
    });
  });
};

module.exports = uploadImgs;