module.exports = function () { // 是否使用缓存
  switch (process.env.NODE_ENV) {
    case 'prod':
    case 'test': return true;
    case 'dev':
    default: return false;
  }
};