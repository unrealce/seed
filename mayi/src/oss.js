/**
 *  北京：
 *    生产：
 *      OSS外网域名: amandahelloseed.oss-cn-beijing.aliyuncs.com
 *      OSS内网域名: amandahelloseed.oss-cn-beijing-internal.aliyuncs.com
 *    测试：
 *      OSS外网域名: amandahelloseedtest.oss-cn-beijing.aliyuncs.com
 *      OSS内网域名: amandahelloseedtest.oss-cn-beijing-internal.aliyuncs.com
 *  美国：
 *    生产：
 *      OSS外网域名: amandahelloseed-us.oss-us-west-1.aliyuncs.com
 *      OSS内网域名: amandahelloseed-us.oss-us-west-1-internal.aliyuncs.com
 *    测试：
 *      OSS外网域名: amandahelloseedtest-us.oss-us-west-1.aliyuncs.com
 *      OSS内网域名: amandahelloseedtest-us.oss-us-west-1-internal.aliyuncs.com
 */
var ALY = require('aliyun-sdk');
var oss = new ALY.OSS({
  accessKeyId: 'Ttbz4jzcjDwmskXV',
  secretAccessKey: '6iDOCRrJBeTyVrswjfT1NL9hR91oAr',
  endpoint: 'http://oss-' + process.env.OSS_REGION + '-internal.aliyuncs.com',
  apiVersion: '2013-10-15'
});
oss.bucket = process.env.OSS_BUCKET;
oss.getUrl = 'http://' + oss.bucket + '.oss-' + process.env.OSS_REGION + '.aliyuncs.com';
oss.upload = function (type, body, key, callback) {
  oss.putObject(
    {
      Bucket: oss.bucket,
      ContentType: type,
      Body: body,
      Key: key
    },
    callback
  ); // 上传
};
module.exports = oss;