var TIMEOUT = 3000;

/**
 *  日志
 *  trace 跟踪
 *  debug 调试
 *  info  信息
 *  warn  警告
 *  error 错误
 *  fatal 致命
 */
var log4js = require('log4js'); // Port of Log4js to work with node
var logger = log4js.getLogger('parse');
logger.setLevel('INFO');
var pmx = require('pmx'); // Keymetrics++ and PM2 adapter

/**
 *  string contains methods that aren't included in the vanilla JavaScript string such as escaping html, decoding html entities, stripping tags, etc.
 */
var S = require('string');
var isUrl = require('is-url'); // Check whether a string is a URL
var read = require('read-comfortably'); // turns any web page into a clean view for reading
var parseDomain = require('parse-domain'); // Splits an url into sub-domain, domain and top-level-domain

var options = require('./options.js'); // read 参数
var leancloud = require('./leancloud.js'); // leancloud rest api
var Article = require('../../model/mongodb/Article.js')(); // 文章实体

var uploadBuffer = require('./upload/buffer.js'); // 图片网站
var uploadHtml = require('./upload/html.js'); // 网页模式
var uploadLink = require('./upload/link.js'); // 阅读模式

var queueName = process.env.MQ_QUEUE; // 队列名称

/**
 *  This is a client for RabbitMQ (and maybe other servers?).
 *  It partially implements the 0.9.1 version of the AMQP protocol.
 */
var amqp = require('amqp'); // AMQP driver for node
var connection = amqp.createConnection({ url: process.env.MQ_URL });
connection.on('ready', function () { // Wait for connection to become established
  logger.info('Connection is ready');
  connection.queue(queueName, function (queue) {
    logger.info('Queue ' + queue.name + ' is open');
    queue.subscribe(function (message) {
      var start = new Date(message.start);
      var geo = message.geo;
      var url = geo.url;
      logger.info('Got url[%s] with:', url, new Date() - start);
      logger.debug('geo:', geo);
      if (!isUrl(url)) {
        logger.warn('%s is not a url', url);
        return;
      }
      findUrl(geo, start);
    });
  });
});

/**
 *  放回队列（重新排队）
 */
var pushAgain = function (doc) {
  var geo = { url: doc.link, countries: doc.countries, force: doc.force };
  logger.warn('push again:', geo);
  connection.publish(queueName, { geo: geo, start: new Date() });
};

/**
 *  查找链接
 */
var findUrl = function (geo, start) {
  var link = geo.url;
  Article.find({ link: link }, function (err, docs) {
    var millis = new Date() - start;
    logger.info('get items [%s] time:', link, millis);
    if (millis > TIMEOUT) {
      pmx.notify({ event: 'helloseed api timeout', error: 'get items [' +  link + '] timeout:' + millis });
    }
    if (err) {
      pmx.notify({ event: 'find article', error: 'find articles error:' + err });
      logger.fatal('find articles error:', err);
    }
    if (!docs || !docs.length) {
      var article = new Article(); // 文章实例
      article.link = link;
      article.countries = geo.countries;
      /**
       *  解析 domain
       */
      var pd = parseDomain(link.replace('@', ''));
      if (pd) {
        article.linkSubdomain = pd.subdomain;
        article.linkDomain = pd.domain;
        article.linkTld = pd.tld;
      }
      article.pubDate = new Date(); // 发布时间
      article.createDate = new Date(); // 创建时间
      // 填充字段
      article.save(function (err, results) {
        if (err) {
          pmx.notify({ event: 'save article', error: 'save [' + article + '] error:' + err });
          logger.fatal('save [%s] error:', article, err);
        }
        logger.trace('saveed:', results);
        findUrl(geo, start); // 查找链接
      });
      return;
    }
    logger.info('find ' + docs.length + ' articles from:', link);
    logger.debug('articles\' docs:', docs);
    docs.forEach(function (doc) {
      logger.debug(link + '\'s doc:', doc);
      doc.force = geo.force;
      doc.userId = geo.userId;
      doc.countries = geo.countries;
      readUrl(doc, start); // 读取链接
    });
  });
};

/**
 *  读取链接
 */
var readUrl = function (item, start) {
  var id = item.id;
  var link = item.link;
  var userId = item.userId;
  var data = {
    id: id,
    url: link,
    htmlUrl: item.htmlUrl,
    linkUrl: item.linkUrl
  };
  if (item.htmlUrl && item.linkUrl && !item.force) {
    logger.info('%s is ready:', id, link);
    // 通知用户文章可阅读了
    data.alert = '您提交的链接解析完成：' + link;
    leancloud.push(userId, data, function (err, res) {
      if (err) {
        pmx.notify({ event: 'leancloud api', error: 'notification user[' + userId + '] error:' + err });
        logger.fatal('notification user[%s] error:', userId, err);
        return;
      }
      logger.trace('notification %s[%s] success:', id, link, res);
    });
    return;
  }
  read(link, options(item.linkDomain), function (err, article, res) {
    logger.info('link:', link);
    var errMsg; // 错误信息
    if (err) {
      errMsg = 'read article ' + id + '[' +  link + '] error: ' + err;
    } else if (res.status != 200) {
      errMsg = 'read article ' + id + '[' +  link + '] status: ' + res.status;
    } else if (!article) {
      errMsg = 'article ' + id + ' is empty: ' + link;
    }
    if (errMsg) { // 遇到以上错误
      pmx.notify({ event: 'read', error: errMsg });
      logger.error(errMsg);
      if (res && 400 < res.status && res.status < 500 && !item.linkUrl) {
        item.remove(function (err, results) {
          if (err) {
            pmx.notify({ event: 'remove article', error: 'save [' + item + '] error:' + err });
            logger.fatal('remove [%s] error:', item, err);
            return;
          }
          logger.trace('removed:', results);
        });
      }
      // 通知用户文章解析失败
      data.alert = '您提交的链接解析失败：' + link;
      leancloud.push(userId, data, function (err, res) {
        if (err) {
          pmx.notify({ event: 'leancloud api', error: 'notification user[' + userId + '] error:' + err });
          logger.fatal('notification user[%s] error:', userId, err);
          return;
        }
        logger.trace('notification %s[%s] success:', id, link, res);
      });
      return;
    }
    var millis = new Date() - start;
    logger.info('read url[%s] time:', link, millis);
    if (millis > TIMEOUT) {
      pmx.notify({ event: 'helloseed mayi timeout', error: 'read url[' +  link + '] timeout:' + millis });
    }
    logger.trace('article:', article);
    var contentType = res.responseHeaders['content-type'];
    if (/html/.test(contentType)) {
      if (!item.title) {
        item.title = S(article.title).stripTags().decodeHTMLEntities().trim().s;
      }
      uploadHtml(item, article, contentType, start, pushAgain); // 网页模式
      uploadLink(item, article, contentType, start, pushAgain); // 阅读模式
    } else {
      pmx.notify({ event: 'warn', error: 'article ' + id + ' content is not html:' + link });
      logger.warn('article %s content is not html:', id, link);
      return uploadBuffer(item, article.content ? article.content : article, contentType, start, pushAgain); // 图片网站
    }
  });
};