var getMongoose = require('./Mongoose');
var ArticleSchema = require('./ArticleSchema');
var getArticle = function (mongoUrl) {
  var mongoose = getMongoose(mongoUrl);
  var Article = mongoose.model('items', ArticleSchema);
  return Article;
};
module.exports = getArticle;