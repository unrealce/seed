var TIMEOUT = 3000;

/**
 *  日志
 *  trace 跟踪
 *  debug 调试
 *  info  信息
 *  warn  警告
 *  error 错误
 *  fatal 致命
 */
var log4js = require('log4js'); // Port of Log4js to work with node
var logger = log4js.getLogger('link');
logger.setLevel('INFO');
var pmx = require('pmx'); // Keymetrics++ and PM2 adapter

/**
 *  string contains methods that aren't included in the vanilla JavaScript string such as escaping html, decoding html entities, stripping tags, etc.
 */
var S = require('string');
var gm = require('gm').subClass({ imageMagick: true }); // GraphicsMagick and ImageMagick for node.js
var md5 = require('md5'); // js function for hashing messages with MD5
var sizeOf = require('image-size'); // get dimensions of any image file
var urlParse = require('url').parse; // Take a URL string, and return an object
var cheerio = require('cheerio'); // Tiny, fast, and elegant implementation of core jQuery designed specifically for the server

var oss = require('../oss.js'); // 阿里云 OSS
var leancloud = require('../leancloud.js'); // leancloud rest api
var Article = require('../../../model/mongodb/Article.js')(); // 文章实体

var uploadImg = require('./img.js'); // 标题图片

var client = process.env.SEED_CLIENT; // 接口地址

/**
 *  阅读模式
 */
var uploadLink = function (item, article, contentType, start, pushAgain) {
  if (item.linkUrl && !item.force) {
    return;
  }
  var id = item.id;
  var link = item.link;
  var content = article.content;
  logger.debug('%s[%s] content:', id, link, content);
  if (article.isEmpty(content)) { // 原文为空
    pmx.notify({ event: 'warn', error: 'article ' + id + ' content is empty:' + link });
    logger.warn('article %s content is empty:', id, link);
    // 通知用户阅读模式原文内容为空
    var data = {
      id: id,
      url: link,
      htmlUrl: item.htmlUrl,
      linkUrl: item.linkUrl,
      alert: '您提交的链接阅读模式解析失败：' + link
    };
    leancloud.push(item.userId, data, function (err, res) {
      if (err) {
        pmx.notify({ event: 'leancloud api', error: 'notification user[' + item.userId + '] error:' + err });
        logger.fatal('notification user[%s] error:', item.userId, err);
        return;
      }
      logger.trace('notification %s[%s] success:', id, link, res);
    });
    return;
  }
  readIfms(item, article, contentType, start, pushAgain); // 读取 iframes
};

/**
 *  读取 iframes
 */
var readIfms = function (item, article, contentType, start, pushAgain) {
  var id = item.id;
  var link = item.link;
  article.$$ = cheerio.load(article.content);
  article.getIframes(function (err, iframes) {
    var millis = new Date() - start;
    logger.info('read article\'s iframes %s[%s] time:', id, link, millis);
    if (millis > TIMEOUT) {
      pmx.notify({ event: 'helloseed mayi timeout', error: 'read article\'s iframes ' + id + '[' +  link + '] timeout:' + millis });
    }
    if (err) {
      pmx.notify({ event: 'read', error: 'read article\'s iframes ' + id + '[' +  link + '] error:' + err });
      logger.error('read article\'s iframes %s[%s] error:', id, link, err);
      return readImgs(item, article, contentType, start, pushAgain); // 读取 images
    }
    var count = iframes.length;
    logger.info('%s find %d iframes', link, count);
    logger.debug('articles\' iframes:', iframes);
    if (!count) {
      pmx.notify({ event: 'read', error: 'read article\'s iframes ' + id + ' is empty:' + link });
      logger.warn('read article\'s iframes %s is empty:', id, link);
      return readImgs(item, article, contentType, start, pushAgain); // 读取 images
    }
    iframes.forEach(function (iframe) {
      logger.debug('iframe:', iframe);
      /**
       *  上传 iframe
       */
      if (!iframe.buf || !iframe.ifmType) {
        if (!--count) {
          return readImgs(item, article, contentType, start, pushAgain); // 读取 images
        }
      }
      var images = iframe.imgs;
      if (!images || !images.length) {
        return uploadIfm(iframe, --count, 0, item, article, contentType, start, pushAgain); // 上传 iframe
      }
      var imgCount = images.length;
      logger.info('%s find %d images', iframe.url, imgCount);
      logger.debug('articles\' images:', images);
      iframe.$ = cheerio.load(iframe.buf);
      images.forEach(function (image) {
        logger.debug('image:', image);
        /**
         *  上传 image
         */
        var type = image.imgType;
        var buf = image.buf;
        if (!buf || !type || /svg/.test(type)) {
          return uploadIfm(iframe, --imgCount ? count : --count, imgCount, item, article, contentType, start, pushAgain); // 上传 iframe
        }
        var url = image.url;
        var isGif = /gif/.test(type);
        var pathname = urlParse(url).pathname;
        var key = 'images/' + id + '/' + md5(pathname);
        logger.trace('image key:', key);
        var file = gm(buf);
        logger.trace('%s image file:', id, file);
        if (!isGif) {
          file.flatten(); // flatten a sequence of images
        }
        if (image.width > 640) {
          file.resize(640); // 缩放
        }
        file.quality(40); // 质量压缩
        file.toBuffer(isGif ? 'GIF' : 'JPEG', function (err, buffer) {
          if (err) {
            pmx.notify({ event: 'gm', error: key + ' gm[' + url + '] to buffer error:' + err });
            logger.error('%s[%s] gm to buffer error:', key, url, err);
            return uploadIfm(iframe, --imgCount ? count : --count, imgCount, item, article, contentType, start, pushAgain); // 上传 iframe
          }
          oss.upload(type, buffer, key, function (err, data) { // 上传 image 到 OSS
            if (err) {
              pmx.notify({ event: 'oss put object', error: 'upload [' + key + '] error:' + err });
              logger.error('upload [%s] error:', key, err);
              return uploadIfm(iframe, --imgCount ? count : --count, imgCount, item, article, contentType, start, pushAgain); // 上传 iframe
            }
            logger.trace('upload [%s] success:', key, data);
            image.ossUrl = oss.getUrl + '/' + key;
            var img = iframe.$('img[src="' + url + '"]');
            img.attr('src', image.ossUrl);
            // var dimensions = sizeOf(buffer);
            // if (dimensions) {
            //   img.attr('width', dimensions.width);
            //   img.attr('height', dimensions.height);
            // }
            img.attr('width', '100%');
            return uploadIfm(iframe, --imgCount ? count : --count, imgCount, item, article, contentType, start, pushAgain); // 上传 iframe
          });
        });
      });
    });
  });
};

/**
 *  上传 iframe
 */
var uploadIfm = function (iframe, count, imgCount, item, article, contentType, start, pushAgain) {
  var id = item.id;
  var url = iframe.url;
  if (imgCount) {
    return logger.trace('%s[%s] iframe images count:', id, url, imgCount);
  }
  var pathname = urlParse(url).pathname;
  var key = 'iframes/' + id + '/' + md5(pathname);
  logger.trace('iframe key:', key);
  oss.upload(iframe.ifmType, iframe.$ ? iframe.$.html() : iframe.buf, key, function (err, data) { // 上传 iframe 到 OSS
    if (err) {
      pmx.notify({ event: 'oss put object', error: 'upload [' + key + '] error:' + err });
      logger.error('upload [%s] error:', key, err);
    } else {
      logger.trace('upload [%s] success:', key, data);
      var ifm = article.$$('iframe[src="' + url + '"]');
      ifm.attr('src', oss.getUrl + '/' + key);
      ifm.attr('orig-src', url);
    }
    if (!count) {
      return readImgs(item, article, contentType, start, pushAgain); // 读取 images
    }
  });
};

/**
 *  读取 images
 */
var readImgs = function (item, article, contentType, start, pushAgain) {
  var id = item.id;
  var link = item.link;
  var $ = article.$$;
  article.getImages(function (err, images) {
    var millis = new Date() - start;
    logger.info('read article\'s images %s[%s] time:', id, link, millis);
    if (millis > TIMEOUT) {
      pmx.notify({ event: 'helloseed mayi timeout', error: 'read article\'s images ' + id + '[' +  link + '] timeout:' + millis });
    }
    if (err) {
      pmx.notify({ event: 'read', error: 'read article\'s images ' + id + '[' +  link + '] error:' + err });
      logger.error('read article\'s images %s[%s] error:', id, link, err);
      return uploadMain(0, item, $.html(), contentType, start, images, pushAgain); // 上传原文
    }
    var count = images.length;
    logger.info('%s find %d images', link, count);
    logger.debug('articles\' images:', images);
    if (!count) {
      pmx.notify({ event: 'read', error: 'read article\'s images ' + id + ' is empty:' + link });
      logger.warn('read article\'s images %s is empty:', id, link);
      return uploadMain(0, item, $.html(), contentType, start, images, pushAgain); // 上传原文
    }
    images.forEach(function (image) {
      logger.debug('image:', image);
      /**
       *  上传 image
       */
      var type = image.imgType;
      var buf = image.buf;
      if (!buf || !type || /svg/.test(type)) {
        return uploadMain(--count, item, $.html(), contentType, start, images, pushAgain); // 上传原文
      }
      var url = image.url;
      var isGif = /gif/.test(type);
      var pathname = urlParse(url).pathname;
      var key = 'images/' + id + '/' + md5(pathname);
      logger.trace('image key:', key);
      var file = gm(buf);
      logger.trace('%s image file:', id, file);
      if (!isGif) {
        file.flatten(); // flatten a sequence of images
      }
      if (image.width > 640) {
        file.resize(640); // 缩放
      }
      file.quality(40); // 质量压缩
      file.toBuffer(isGif ? 'GIF' : 'JPEG', function (err, buffer) {
        if (err) {
          pmx.notify({ event: 'gm', error: key + ' gm[' + url + '] to buffer error:' + err });
          logger.error('%s[%s] gm to buffer error:', key, url, err);
          return uploadMain(--count, item, $.html(), contentType, start, images, pushAgain); // 上传原文
        }
        oss.upload(type, buffer, key, function (err, data) { // 上传 image 到 OSS
          if (err) {
            pmx.notify({ event: 'oss put object', error: 'upload [' + key + '] error:' + err });
            logger.error('upload [%s] error:', key, err);
            return uploadMain(--count, item, $.html(), contentType, start, images, pushAgain); // 上传原文
          }
          logger.trace('upload [%s] success:', key, data);
          image.ossUrl = oss.getUrl + '/' + key;
          var img = $('img[src="' + url + '"]');
          img.attr('src', image.ossUrl);
          var dimensions = sizeOf(buffer);
          if (dimensions) {
            img.attr('width', dimensions.width);
            img.attr('height', dimensions.height);
          }
          return uploadMain(--count, item, $.html(), contentType, start, images, pushAgain); // 上传原文
        });
      });
    });
  });
};

/**
 *  上传原文
 */
var uploadMain = function (count, item, content, type, start, images, pushAgain) {
  var id = item.id;
  var link = item.link;
  if (count) {
    return logger.trace('%s[%s] images count:', id, link, count);
  }
  var body;
  var text = S(content).stripTags().decodeHTMLEntities().trimLeft();
  if (typeof content == 'string' && text.indexOf(item.title) == -1) { // 原文中不包含标题，加上它
    if (item.linkDomain == 'instagram') {
      body = content + '<p>' + item.title + '</p>';
    } else {
      body = '<h2>' + item.title + '</h2>' + content;
    }
  } else {
    body = content;
  }
  body = '<head><title>' + item.title + '</title></head>' + '<body>' + body + '</body>';
  /**
   *  上传原文到 OSS
   */
  var key = 'main/' + id;
  logger.trace('main key:', key);
  logger.trace('main body:', body);
  logger.trace('main content-type:', type);
  oss.upload(type, body, key, function (err, data) {
    if (err) {
      pmx.notify({ event: 'oss put object', error: 'upload [' + key + '] error:' + err });
      logger.error('upload [%s] error:', key, err);
      return pushAgain(item);
    }
    logger.trace('upload [%s] success:', key, data);
    var millis = new Date() - start;
    logger.info('upload %s[%s] time:', key, link, millis);
    if (millis > TIMEOUT) {
      pmx.notify({ event: 'helloseed mayi timeout', error: 'upload ' + key + '[' +  link + '] timeout:' + millis });
    }
    /**
     *  更新原文信息到 article
     */
    logger.info('update article for main:', id);
    var updateData = {
      title: item.title,
      mainUrl: oss.getUrl + '/' + key,
      linkUrl: client + 'cfs/files/sources/' + id,
      countries: item.countries,
      updateDate: new Date()
    };
    if (!item.description) {
      updateData.description = text.truncate(300).s;
    }
    if (item.linkDomain == 'instagram') {
      var index = item.title.search(/[,.?!]/);
      if (index != -1) {
        updateData.title = item.title.substring(0, index);
        updateData.description = item.title.substring(index + 1);
      }
    }
    var image = getImage(item, images); // 获取 image (封面)
    if (images && images.length) {
      updateData.images = images;
    }
    if (image) {
      image._id = id;
      updateData.image = image;
    }
    Article.findByIdAndUpdate(id, { $set: updateData }, function (err, doc) {
      if (err) {
        pmx.notify({ event: 'update article', error: 'update article[' + id + '] error:' + err });
        logger.fatal('update article[%s] error:', id, err);
        return pushAgain(item);
      }
      logger.trace('update article[%s] success:', id, doc);
      // 通知用户阅读模式原文可阅读了
      var data = {
        id: id,
        url: link,
        htmlUrl: item.htmlUrl,
        linkUrl: item.linkUrl,
        alert: '您提交的链接阅读模式解析完成：' + link
      };
      leancloud.push(item.userId, data, function (err, res) {
        if (err) {
          pmx.notify({ event: 'leancloud api', error: 'notification user[' + item.userId + '] error:' + err });
          logger.fatal('notification user[%s] error:', item.userId, err);
          return;
        }
        logger.trace('notification %s[%s] success:', id, link, res);
      });
      var millis = new Date() - start;
      logger.info('update main %s[%s] time:', id, link, millis);
      if (millis > TIMEOUT) {
        pmx.notify({ event: 'helloseed mayi timeout', error: 'update main ' + id + '[' +  link + '] timeout:' + millis });
      }
      uploadImg(image); // 标题图片
    });
  });
};

/**
 *  获取 image (封面)
 */
var getImage = function (item, images) {
  var biggest;
  if (item.image && item.image.url) {
    biggest = item.image;
  }
  if (!biggest && images) {
    images.forEach(function (image) {
      if (!image.buf || image.width < 200 || image.height < 200) {
        return;
      }
      if (!biggest || biggest.buf.length < image.buf.length) {
        biggest = image;
      }
    });
  }
  return biggest ? calcSize(biggest) : null;
};

/**
 *  计算 image 新尺寸
 */
var MIN_RATE = 225 / 350; // 最小宽高比
var MAX_RATE = 225 / 160; // 最大宽高比
var calcSize = function (image) {
  if (image.width && image.height) {
    image.imgWidth = image.width;
    image.imgHeight = image.height;
    var rate = image.width / image.height; // 图片实际宽高比
    if (rate < MIN_RATE) { // 小于最小宽高比
      image.height = image.width / MIN_RATE; // 截取最大高度
    } else if (rate > MAX_RATE) { // 大于最大宽高比
      image.width = image.height * MAX_RATE; // 截取最大宽度
    }
  }
  return image;
};

module.exports = uploadLink;