var TIMEOUT = 100;

/**
 *  日志
 *  trace 跟踪
 *  debug 调试
 *  info  信息
 *  warn  警告
 *  error 错误
 *  fatal 致命
 */
var log4js = require('log4js'); // Port of Log4js to work with node
var logger = log4js.getLogger('read');
logger.setLevel('INFO');
var pmx = require('pmx'); // Keymetrics++ and PM2 adapter

var Article = require('../../../model/mongodb/Article.js')(); // 文章实体

var header = { 'Content-Type': 'text/plain; charset=utf-8', 'Access-Control-Allow-Origin': '*' }; // 返回头部信息

/**
 *  阅读数
 */
var readAct = function (req, res) {
  var start = new Date();
  var id = req.query.id;
  logger.trace('id:', id);
  Article.findOne({ _id: id }, 'readCount realReadCount', function (err, doc) {
    logger.info('article:', doc);
    if (err) {
      pmx.notify({ event: 'find article', error: 'find article[' + id + '] error:' + err });
      logger.fatal('find article[%s] error:', id, err);
      res.writeHead(500, header);
      res.end(JSON.stringify({ result: false, error_code: 500, msg: 'Find Error' }));
      return;
    }
    if (!doc) {
      pmx.notify({ event: 'find article', error: 'find article[' + id + '] empty' });
      logger.fatal('find article[%s] empty', id);
      res.writeHead(404, header);
      res.end(JSON.stringify({ result: false, error_code: 404, msg: 'Not Found' }));
      return;
    }
    Article.findByIdAndUpdate(id, { $inc: { readCount: parseInt(Math.random() * 50), realReadCount: 1 } }, function (err, doc) {
      if (err) {
        pmx.notify({ event: 'update article', error: 'update article[' + id + '] error:' + err });
        logger.fatal('update article[%s] error:', id, err);
        res.writeHead(500, header);
        res.end(JSON.stringify({ result: false, error_code: 500, msg: 'Update Error' }));
        return;
      }
      res.writeHead(200, header);
      res.end(JSON.stringify({ result: true, error_code: 1 }));
      var millis = new Date() - start;
      logger.info('update item [%s][%s] success:', id, millis, doc);
      if (millis > TIMEOUT) {
        pmx.notify({ event: 'helloseed api timeout', error: 'update item [' +  id + '] timeout:' + millis });
      }
    });
  });
};

module.exports = readAct;