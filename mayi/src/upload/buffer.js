var TIMEOUT = 3000;

/**
 *  日志
 *  trace 跟踪
 *  debug 调试
 *  info  信息
 *  warn  警告
 *  error 错误
 *  fatal 致命
 */
var log4js = require('log4js'); // Port of Log4js to work with node
var logger = log4js.getLogger('buffer');
logger.setLevel('INFO');
var pmx = require('pmx'); // Keymetrics++ and PM2 adapter

var oss = require('../oss.js'); // 阿里云 OSS
var leancloud = require('../leancloud.js'); // leancloud rest api
var Article = require('../../../model/mongodb/Article.js')(); // 文章实体

/**
 *  图片网站
 */
var uploadBuffer = function (item, buffer, type, start, pushAgain) {
  if (item.htmlUrl && item.linkUrl && !item.force) {
    return;
  }
  var id = item.id;
  var link = item.link;
  /**
   *  上传图片到 OSS
   */
  var key = 'main/' + id;
  logger.trace('main key:', key);
  logger.trace('main body:', buffer);
  logger.trace('main content-type:', type);
  oss.upload(type, buffer, key, function (err, data) {
    if (err) {
      pmx.notify({ event: 'oss put object', error: 'upload [' + key + '] error:' + err });
      logger.error('upload [%s] error:', key, err);
      return pushAgain(item);
    }
    logger.trace('upload [%s] success:', key, data);
    var millis = new Date() - start;
    logger.info('upload %s[%s] time:', key, link, millis);
    if (millis > TIMEOUT) {
      pmx.notify({ event: 'helloseed mayi timeout', error: 'upload ' + key + '[' +  link + '] timeout:' + millis });
    }
    /**
     *  更新原文信息到 article
     */
    logger.info('update article for main:', id);
    var updateData = {
      title: item.title,
      htmlUrl: oss.getUrl + '/' + key,
      mainUrl: oss.getUrl + '/' + key,
      linkUrl: oss.getUrl + '/' + key,
      countries: item.countries,
      updateDate: new Date()
    };
    Article.findByIdAndUpdate(id, { $set: updateData }, function (err, doc) {
      if (err) {
        pmx.notify({ event: 'update article', error: 'update [' + id + '] error:' + err });
        logger.fatal('update [%s] error:', id, err);
        return pushAgain(item);
      }
      logger.trace('update [%s] success:', id, doc);
      // 通知用户原文可阅读了
      var data = {
        id: id,
        url: link,
        htmlUrl: item.htmlUrl,
        linkUrl: item.linkUrl,
        alert: '您提交的链接解析完成：' + link
      };
      leancloud.push(item.userId, data, function (err, res) {
        if (err) {
          pmx.notify({ event: 'leancloud api', error: 'notification user[' + item.userId + '] error:' + err });
          logger.fatal('notification user[%s] error:', item.userId, err);
          return;
        }
        logger.trace('notification %s[%s] success:', id, link, res);
      });
      var millis = new Date() - start;
      logger.info('update main %s[%s] time:', id, link, millis);
      if (millis > TIMEOUT) {
        pmx.notify({ event: 'helloseed mayi timeout', error: 'update main ' + id + '[' +  link + '] timeout:' + millis });
      }
    });
  });
};

module.exports = uploadBuffer;