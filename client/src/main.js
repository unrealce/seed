if (process.env.NODE_ENV == 'prod') {
  require('newrelic'); 
}
var express = require('express');
var bodyParser = require('body-parser');

var app = express();
app.use(express.static(__dirname + '/static'));
app.use(bodyParser.urlencoded({ extended: false }));

/**
 *  To get more verbose data when an exception is thrown in route, add this middleware
 */
var pmx = require('pmx');

/**
 *  接口
 */
var itemApi = require('./api/item.js');
var itemsApi = require('./api/items.js');
var imageApi = require('./api/images.js');
var sourceApi = require('./api/sources.js');

app.get('/item', itemApi);
app.post('/item', itemApi);
app.get('/items', itemsApi);
app.post('/items', itemsApi);
app.get('/cfs/files/images/:id', imageApi);
app.get('/cfs/files/sources/:id', sourceApi);

/**
 *  操作
 */
var readAct = require('./act/read.js'); // 阅读数
var parseAct = require('./act/parse.js'); // 解析URL

app.get('/read', readAct);
app.get('/parse', parseAct);
app.post('/parse', parseAct);

/**
 *  剩余统一返回运行信息
 */
app.all('*', function (req, res) {
  res.send('helloseed api running');
});

/**
 *  Add the error middleware at the end (after route declaration)
 */
app.use(pmx.expressErrorHandler());

app.listen(3001);