var TIMEOUT = 20; // the api timeout for one item (in milliseconds).

/**
 *  日志
 *  trace 跟踪
 *  debug 调试
 *  info  信息
 *  warn  警告
 *  error 错误
 *  fatal 致命
 */
var log4js = require('log4js'); // Port of Log4js to work with node
var logger = log4js.getLogger('items');
logger.setLevel('INFO');
var pmx = require('pmx'); // Keymetrics++ and PM2 adapter
var md5 = require('md5'); // js function for hashing messages with MD5
var request = require('request'); // Simplified HTTP request client

var USE_CACHE = require('../../../model/memcache/useCache.js')(); // 是否使用缓存
var memcached = require('../../../model/memcache/Memcached.js'); // 缓存服务
var Article = require('../../../model/mongodb/Article.js')(); // 文章实体
var projectionDefault = require('../common/constants.js').projectionDefault; // 默认返回字段

var header = { 'Content-Type': 'text/plain; charset=utf-8', 'Access-Control-Allow-Origin': '*' }; // 返回头部信息

/**
 *  获取 articles 接口
 */
var itemsApi = function (req, res) {
  var id = getParam(req, 'id'); // 文章 id
  var url = getParam(req, 'url'); // 文章 url
  var next = getParam(req, 'next'); // 仅为 true 时查询更新的文章
  var sourceType = getParam(req, 'sourceType'); // 分类
  var sourceName = getParam(req, 'sourceName'); // 名称
  if (typeof sourceName == 'undefined') {
    sourceName = sourceType;
  }
  var linkDomain = getParam(req, 'linkDomain'); // 域名
  var skip = parseInt(getParam(req, 'skip'));
  var limit = parseInt(getParam(req, 'limit'));
  var projection = getParam(req, 'projection'); // 返回字段
  if (!projection) {
    projection = projectionDefault; // 如果没传返回默认
  } else if (projection == 'all') {
    projection = ''; // 如果为 all 返回所有
  }
  var useCache = USE_CACHE & limit < 80; // 是否使用缓存
  var timeout = TIMEOUT * limit; // 重置超时时间
  if (useCache) { // 使用缓存
    var start = new Date();
    var key = initCacheKey(id, url, next, sourceType, sourceName, linkDomain, skip, limit, projection); // 生成缓存键值
    memcached.get(key, function (err, data) { // 查询缓存
      if (err) {
        pmx.notify({ event: 'memcached', error: 'memcached get [' +  key + '] error:' + err });
        logger.error('memcached get [%s] error:', key, err);
      } else {
        logger.info('memcached key:', key);
        logger.debug('memcached data:', data);
        if (data) { // 缓存数据
          res.writeHead(200, header);
          res.end(data);
          var millis = new Date() - start;
          if (millis > timeout) {
            pmx.notify({ event: 'helloseed api timeout', error: 'memcached get [' +  key + '] timeout:' + millis });
          }
          logger.info('memcached get [%s] time:', key, millis);
          return;
        }
      }
      initSearch(res, id, url, next, sourceType, sourceName, linkDomain, skip, limit, projection, key); // 搜索 articles
    });
  } else { // 不使用缓存直接查询数据库
    initSearch(res, id, url, next, sourceType, sourceName, linkDomain, skip, limit, projection); // 搜索 articles
  }
};

/**
 *  构造搜索条件
 */
var initSearch = function (res, id, url, next, sourceType, sourceName, linkDomain, skip, limit, projection, key) {
  var selector = { description: { $ne: '' }, linkUrl: { $ne: null } };
  selector.linkDomain = { $not: { $in: ['youtube'] } }; // 排除视频网站
  logger.trace('url:', url);
  if (url) {
    selector.link = url; // 类型条件
  }
  logger.trace('sourceType:', sourceType);
  if (sourceType) {
    selector.sourceType = { $in: sourceType.split(',') }; // 类型条件
  }
  logger.trace('sourceName:', sourceName);
  if (sourceName) {
    selector.sourceName = { $in: sourceName.split(',') }; // 名称条件
  }
  logger.trace('linkDomain:', linkDomain);
  if (linkDomain) {
    selector.linkDomain = { $in: linkDomain.split(',') }; // 域名条件
  }
  logger.trace('skip:', skip);
  logger.trace('limit:', limit);
  var options = { sort: { pubDate: -1 }, skip: skip, limit: limit }; // 排序&分页
  logger.trace('id:', id);
  if (!id) {
    return searchItems(res, options, selector, limit, projection, key);
  }
  Article.findOne({ _id: id }, 'pubDate', function (err, doc) {
    if (err) {
      pmx.notify({ event: 'find article', error: 'find article error:' + err });
      logger.fatal('find article[%s/%s] error:', id, url, err);
      res.sendStatus(500);
      return;
    }
    if (!doc) {
      pmx.notify({ event: 'find article', error: 'find article empty' });
      logger.fatal('find article[%s/%s] empty', id, url);
      res.sendStatus(404);
      return;
    }
    logger.trace('next:', next);
    if (next == 'true') { // 查询更新的文章
      options.sort = { pubDate: 1 };
      selector.pubDate = { $gt: doc.pubDate };
    } else { // 查询更旧的文章
      selector.pubDate = { $lt: doc.pubDate };
    }
    return searchItems(res, options, selector, limit, projection, key);
  });
}

/**
 *  搜索 articles
 */
var searchItems = function (res, options, selector, limit, projection, key) {
  var start = new Date();
  logger.debug('options:', options);
  logger.debug('selector:', selector);
  Article.find(selector, projection, options, function (err, docs) {
    if (err) {
      pmx.notify({ event: 'find article', error: 'find articles error:' + err });
      logger.fatal('find articles error:', err);
      res.sendStatus(500);
      return;
    }
    if (!docs) {
      pmx.notify({ event: 'find article', error: 'find articles empty' });
      logger.fatal('find articles empty');
      res.sendStatus(404);
      return;
    }
    if (options.sort.pubDate == 1) {
      docs.reverse();
    }
    var count = docs.length;
    logger.info('find ' + count + ' articles');
    if (!count || projection.search('content') == -1) {
      return cacheResult(res, options, selector, limit, start, docs, key);
    }
    logger.debug('articles\' data:', docs);
    docs.forEach(function (doc) { // 加载原文
      var id = doc._id;
      var url = doc.linkUrl;
      request(url, { timeout: 1000 }, function (err, resp, body) {
        var errMsg; // 错误信息
        if (err) {
          errMsg = 'request ' + id + '[' +  url + '] error:' + err;
        } else if (resp.statusCode != 200) {
          errMsg = 'request ' + id + '[' +  url + '] statusCode:' + resp.statusCode;
        } else if (!body) {
          errMsg = 'request ' + id + '[' +  url + '] body empty';
        }
        if (errMsg) {
          pmx.notify({ event: 'request', error: errMsg });
          logger.error(errMsg);
        } else {
          doc.content = body;
        }
        if (!--count) {
          cacheResult(res, options, selector, limit, start, docs, key);
        }
      });
    });
  });
};

/**
 *  缓存结果
 */
var cacheResult = function (res, options, selector, limit, start, docs, key) {
  var millis = new Date() - start;
  var result = {}; // 结果
  result.rows = docs; // articles
  result.rows_length = docs.length; // 数量
  result.query = selector; // 查询条件
  result.opt = options; // 排序&分页
  result.millis = millis; // 查询时间
  res.writeHead(200, header);
  res.end(JSON.stringify(result));
  selector.options = options;
  logger.info('get items [%s] time:', JSON.stringify(selector), millis);
  if (millis > limit * TIMEOUT) {
    pmx.notify({ event: 'helloseed api timeout', error: 'get items [' +  JSON.stringify(selector) + '] timeout:' + millis });
  }
  if (key) { // 使用缓存
    memcached.set(key, JSON.stringify(result), 300, function (err) { // 缓存5分钟
      if (err) {
        pmx.notify({ event: 'memcached', error: 'memcached set [' +  key + '] error:' + err });
        logger.fatal('memcached set [%s] error:', key, err);
        return;
      }
      logger.trace('cached:', key);
    });
  }
};

/**
 *  生成缓存键值
 */
var initCacheKey = function (id, url, next, sourceType, sourceName, linkDomain, skip, limit, projection) {
  var keyStr = '';
  if (id) { keyStr += id; }
  if (url) { keyStr += url; }
  if (next) { keyStr += next; }
  if (sourceType) { keyStr += sourceType; }
  if (sourceName) { keyStr += sourceName; }
  if (linkDomain) { keyStr += linkDomain; }
  keyStr += skip ? skip : 0;
  keyStr += limit ? limit : 0;
  if (projection) { keyStr += projection; }
  logger.debug('memcached key str:', keyStr);
  return md5(keyStr);
};

/**
 *  获取参数
 */
var getParam = function (req, name) {
  var value = req.query[name];
  if (typeof value != 'undefined') {
    return value;
  }
  value = req.body[name];
  if (typeof value != 'undefined') {
    return value;
  }
  return req.params[name];
};

module.exports = itemsApi;