var mongoose = require('mongoose');
var getMongoose = function (mongoUrl) {
  if (!mongoUrl) {
    mongoUrl = process.env.MONGO_URL;
  }
  return mongoose.createConnection(mongoUrl);
};
module.exports = getMongoose;