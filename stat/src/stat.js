/**
 *  日志
 *  trace 跟踪
 *  debug 调试
 *  info  信息
 *  warn  警告
 *  error 错误
 *  fatal 致命
 */
var log4js = require('log4js'); // Port of Log4js to work with node
var logger = log4js.getLogger('stat');
logger.setLevel('INFO');
var pmx = require('pmx'); // Keymetrics++ and PM2 adapter
var dateFormat = require('dateformat'); // A node.js package for Steven Levithan's excellent dateFormat() function.

var Article = require('../../model/mongodb/Article.js')(); // 文章实体

module.exports = function(req, res) {
  var start = new Date(req.body.start);
  var end = new Date(req.body.end);
  var field = req.body.field;
  var unit = req.body.unit;
  logger.trace('start:', start);
  logger.trace('end:', end);
  logger.trace('unit:', unit);
  logger.trace('field:', field);
  var format;
  switch(unit) {
    case 'Hour':
    case 'Day':
      format = 'yyyy-mm-dd';
      break;
    case 'Month':
      format = 'yyyy-mm';
      break;
    case 'Year':
      format = 'yyyy';
      break;
  }
  var startDate = new Date(dateFormat(start, format));
  var endDate = new Date(dateFormat(end, format));
  dateInc(endDate, unit == 'Hour' ? 'Day' : unit);
  var selector = { pubDate: { $gte: startDate, $lt: endDate } };
  logger.info('selector:', selector);  
  Article.find(selector, 'pubDate ' + field, function (err, docs) {
    if (err) {
      pmx.notify({ event: 'find article', error: 'find articles error:' + err });
      logger.fatal('find articles error:', err);
      res.sendStatus(500);
      return;
    }
    if (!docs) {
      pmx.notify({ event: 'find article', error: 'find articles empty' });
      logger.fatal('find articles empty');
      res.sendStatus(404);
      return;
    }
    logger.info('find ' + docs.length + ' articles');
    logger.debug('articles\' data:', docs);
    switch(unit) {
      case 'Hour':
        start.setHours(0);
        if (end < new Date(dateFormat(new Date(), 'yyyy-mm-dd'))) {
          end.setHours(23);
        }
        format = 'dd HH';
        break;
      case 'Day':
        format = 'mm-dd';
        break;
    }
    var categories = [];
    var seriesMap = {};
    var sum = 0;
    do {
      categories.push(dateFormat(start, format));
      dateInc(start, unit);
    } while (start <= end);
    var total = new Array(categories.length);
    docs.forEach(function (doc) {
      var index = categories.indexOf(dateFormat(doc.pubDate, format, true));
      var sourceType = doc[field];
      if (!seriesMap[sourceType]) {
        seriesMap[sourceType] = { data: new Array(categories.length), count: 0 };
      }
      sum++;
      seriesMap[sourceType].count++;
      var data = seriesMap[sourceType].data;
      if (!data[index]) {
        data[index] = 0;
      }
      data[index]++;
      if (!total[index]) {
        total[index] = 0;
      }
      total[index]++;
    });
    logger.debug('seriesMap:', seriesMap);
    var series = [];
    var seriesData = [];
    for (key in seriesMap) {
      series.push({ name: key, data: seriesMap[key].data });
      seriesData.push({ category: key, value: (seriesMap[key].count * 100 / sum).toFixed(2) });
    }
    series.push({
      data: total,
      type: 'line',
      name: 'total',
      style: 'smooth',
      labels: { visible: true }
    });
    logger.info('series:', series);
    var max = 0, maxIndex = 0;
    seriesData.forEach(function (data, index) {
      var value = parseFloat(data.value);
      if (max < value) {
        max = value;
        maxIndex = index;
      }
    });
    if (seriesData[maxIndex]) {
      seriesData[maxIndex].explode = true;
    }
    logger.info('seriesData:', seriesData);
    res.json({
      area: {
        categories: categories,
        series: series,
        unit: unit,
        sum: sum
      },
      pie: {
        series: seriesData
      }
    });
  });
};

var dateInc = function (date, unit) {
  switch(unit) {
    case 'Hour':
      date.setHours(date.getHours() + 1);
      break;
    case 'Day':
      date.setDate(date.getDate() + 1);
      break;
    case 'Month':
      date.setMonth(date.getMonth() + 1);
      break;
    case 'Year':
      date.setFullYear(date.getFullYear() + 1);
      break;
  }
};