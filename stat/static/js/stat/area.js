var createArea = function (data) {
  $('#area').kendoChart({
    title: { text: 'Articles & SourceType By ' + data.unit + ' (' + data.sum + ')' },
    legend: { position: 'bottom' },
    seriesDefaults: {
      type: 'area',
      stack: true,
      area: {
        line: {
          style: 'smooth'
        }
      },
      labels: {
        background: 'transparent'
      }
    },
    series: data.series,
    valueAxis: { line: { visible: false } },
    categoryAxis: {
      categories: data.categories,
      majorGridLines: { visible: false },
      crosshair: { visible: true }
    },
    tooltip: {
      visible: true,
      shared: true
    }
  });  
};