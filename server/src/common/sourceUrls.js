var sourceUrls = { // 抓取源
  'editors-pick': [ // 推荐
    { name: 'editors-pick', url: 'https://flipboard.com/@marinau3nr/editor%E2%80%99s-pick-b478ui8dy' }
  ],
  'the-techy': [ // 科技 Technology
    { name: 'the-verge', url: 'https://flipboard.com/section/the-verge-91pj948t2hin8lj4' },
    { name: 'engadget', url: 'https://flipboard.com/section/engadget-6rppj94ktm38j5kk' },
    { name: 'gizmodo', url: 'https://flipboard.com/section/gizmodo-pqqn8h6bjc44aaut' },
    { name: 'techcrunch', url: 'https://flipboard.com/@techcrunch' }, // https://flipboard.com/section/techcrunch-g0dk3e33all443op
    { name: 'venturebeat', url: 'https://flipboard.com/section/venturebeat-2h6kpfjcrt5pjodu' },
    { name: 'fast-company', url: 'https://flipboard.com/section/fast-company-r2rgo11dh4qg0jds' },
    { name: 'businessinsider', url: 'https://flipboard.com/@businessinsider' }, // https://flipboard.com/section/business-insider-gpav8qvg9s8ip9dd 🌟
    { name: 'mashable', url: 'https://flipboard.com/section/mashable-8ofroouos4qndrfc' },
    { name: 'the-techy', url: 'https://flipboard.com/@marinau3nr/the-techy-p7hnr959y' }
  ],
  'the-trendy': [ // 生活 Lifestyle
    { name: 'lonely-planet', url: 'https://flipboard.com/section/lonely-planet-rbc6egk1ggj2huj3' },
    { name: 'conde-nast-traveler', url: 'https://flipboard.com/section/cond%C3%A9-nast-traveler-94mras3t93bbuvm6' },
    { name: 'food52', url: 'https://flipboard.com/section/food52-d06t2pdm3pmf1cae' },
    { name: 'smitten-kitchen', url: 'https://flipboard.com/section/smitten-kitchen-ur6cc7q99lfgieqt' },
    { name: 'nomadic-matt', url: 'https://flipboard.com/section/nomadic-matt%27s-travel-site-5ls77vio3ioe7c8k' }, // https://flipboard.com/section/nomadic-matt-5ls77vio3ioe7c8k
    { name: 'hypebeast', url: 'https://flipboard.com/@hypebeast' }, // https://flipboard.com/section/hypebeast-9fdidblvehihjq8s
    { name: 'the-sartorialist', url: 'https://flipboard.com/section/the-sartorialist-fcs1laotn7lf0gme' },
    { name: 'uncrate', url: 'https://flipboard.com/section/uncrate-6jkg18skh5bv5kpf' },
    { name: 'business-of-fashion', url: 'https://flipboard.com/section/the-business-of-fashion-cdughdmqcpcsle3t' }, // the-business-of-fashion -> business-of-fashion
    { name: 'the-trendy', url: 'https://flipboard.com/@marinau3nr/the-trendy-2saogstay' }
  ],
  'the-pretty': [ // 审美 Design
    { name: 'fastco.design', url: 'https://flipboard.com/section/fast-company-co.design-ol8l471tpdp64cqk' }, // fast-company-co.design -> fastco.design
    { name: 'design-milk', url: 'https://flipboard.com/section/design-milk-rn3b8uicq2vhc402' },
    { name: 'dezeen', url: 'https://flipboard.com/section/dezeen-4aj6s0s1pcvb10gt' },
    { name: 'core77', url: 'https://flipboard.com/section/core77-7fift1f9g17lq2b6' },
    { name: 'cool-hunting', url: 'https://flipboard.com/section/cool-hunting-9sdd5k3ul2rsmm6m' },
    { name: 'abduzeedo', url: 'https://flipboard.com/section/abduzeedo-design-inspiration---design-inspiration-%26-tutorials-fh8d17gpcquiqmqg' }, // https://flipboard.com/section/abduzeedo-fh8d17gpcquiqmqg
    { name: 'mymodernmet', url: 'https://flipboard.com/@mymodernmet' }, // https://flipboard.com/section/my-modern-met-t84me3a51qbjmsg0
    { name: 'the-pretty', url: 'https://flipboard.com/@marinau3nr/the-pretty-vdif469ry' }
  ],
  'entertainment': [ // 娱乐 -> 社会 Pop culture
    { name: 'io9', url: 'https://flipboard.com/section/io9-j5rrl3jdqlmd6hnr' },
    { name: 'boing-boing', url: 'https://flipboard.com/section/boing-boing-t39qr4u85if2pkd0' },
    { name: 'gawker', url: 'https://flipboard.com/section/gawker-lq0bivpl84nthlr7' },
    { name: 'mental-floss', url: 'https://flipboard.com/section/mental-floss-jir4qbrl8n51m2pb' },
    { name: 'vice', url: 'https://flipboard.com/@vice' }, // https://flipboard.com/section/vice-42klm4m1i22j7i0v
    { name: 'buzzfeed', url: 'https://flipboard.com/section/buzzfeed-qu9af2g3n1r9dju0' },
    { name: 'the-atlantic', url: 'https://flipboard.com/@theatlantic' }, // https://flipboard.com/section/the-atlantic-bgjei8vu0sjmc67i
    { name: 'the-huffington-post', url: 'https://flipboard.com/section/the-huffington-post-nouoch16ab67kko6' }, // 🌟
    { name: 'entertainment', url: 'https://flipboard.com/@marinau3nr/entertainment-bgki4ldpy' }
  ],
  'kickstarter': [ // kickstarter
    { name: 'kickstarter-film', url: 'https://flipboard.com/@kickstarter/kickstarter-x-film-aktnimd7z' },
    { name: 'kickstarter-publishing', url: 'https://flipboard.com/@kickstarter/kickstarter-x-publishing-o4bcsa60z' },
    { name: 'kickstarter-games', url: 'https://flipboard.com/@kickstarter/kickstarter-x-games-qqbjq9jbz' },
    { name: 'kickstarter-food', url: 'https://flipboard.com/@kickstarter/kickstarter-x-food-473hb604z' },
    { name: 'kickstarter-design', url: 'https://flipboard.com/@kickstarter/kickstarter-x-design-uf0b91jtz' },
    { name: 'kickstarter-innovation', url: 'https://flipboard.com/@kickstarter/kickstarter-x-innovation-us52q55hz' },
    { name: 'kickstarter-art', url: 'https://flipboard.com/@kickstarter/kickstarter-x-art-35mppacsz' },
    { name: 'kickstarter', url: 'https://flipboard.com/@kickstarter' },
    { name: 'indiegogo', url: 'https://flipboard.com/topic/indiegogo' }
  ]
};
module.exports = sourceUrls;