var TIMEOUT = 400; // the api timeout for the sources (in milliseconds).

/**
 *  日志
 *  trace 跟踪
 *  debug 调试
 *  info  信息
 *  warn  警告
 *  error 错误
 *  fatal 致命
 */
var log4js = require('log4js'); // Port of Log4js to work with node
var logger = log4js.getLogger('source');
logger.setLevel('INFO');
var pmx = require('pmx'); // Keymetrics++ and PM2 adapter
/*
 *  encoding: null tells request that you want a buffer, not a string.
 *  A string gives you unusably garbled data, as the whole point of base64 is to encode binary.
 */
var request = require('request').defaults({ encoding: null }); // Simplified HTTP request client
var cheerio = require('cheerio'); // Tiny, fast, and elegant implementation of core jQuery designed specifically for the server

var USE_CACHE = require('../../../model/memcache/useCache.js')(); // 是否使用缓存
var memcached = require('../../../model/memcache/Memcached.js'); // 缓存服务
var Article = require('../../../model/mongodb/Article.js')(); // 文章实体
var ossUrl = require('../common/constants.js').ossUrl; // OSS 网址

var server = require('./server.js'); // 前端处理逻辑

var header = { 'Content-Type': 'text/html; charset=utf-8', 'Access-Control-Allow-Origin': '*' }; // 返回头部信息

var client = process.env.SEED_CLIENT; // 接口地址

/**
 *  获取原文接口
 */
var sourceApi = function (req, res) {
  var force = req.query.force ? true : false;
  logger.trace('force:', force);
  var id = req.params.id; // 文章 ID
  logger.debug('id:', id);
  var key = 'main/' + id;
  if (USE_CACHE && !force) { // 使用缓存
    var start = new Date();
    memcached.get(key, function (err, data) { // 查询缓存
      if (err) {
        pmx.notify({ event: 'memcached', error: 'memcached get [' +  key + '] error:' + err });
        logger.error('memcached get [%s] error:', key, err);
      } else {
        logger.info('memcached key:', key);
        logger.debug('memcached data:', data);
        if (data) { // 缓存数据
          res.writeHead(200, header);
          res.end(data);
          var millis = new Date() - start;
          if (millis > TIMEOUT) {
            pmx.notify({ event: 'helloseed api timeout', error: 'memcached get [' +  key + '] timeout:' + millis });
          }
          logger.info('memcached get [%s] time:', key, millis);
          return;
        }
      }
      getSource(res, key, id); // 获取原文
    });
  } else { // 不使用缓存直接获取原文
    getSource(res, key, id); // 获取原文
  }
};

/**
 *  获取原文
 */
var getSource = function (res, key, id) {
  var start = new Date();
  Article.findOne({ _id: id }, 'link mainUrl', function (err, doc) {
    if (err) {
      pmx.notify({ event: 'find article', error: 'find article[' + id + '] error:' + err });
      logger.fatal('find article[%s] error:', id, err);
      return res.sendStatus(500);
    }
    if (!doc) {
      pmx.notify({ event: 'find article', error: 'find article[' + id + '] empty' });
      logger.fatal('find article[%s] empty', id);
      return res.sendStatus(404);
    }
    logger.info('item:', doc);
    var url = doc.mainUrl;
    if (!url) {
      url = ossUrl + key;
    }
    request(url, function (err, resp, buffer) {
      var errMsg; // 错误信息
      if (err) {
        errMsg = 'request ' + id + '[' +  url + '] error:' + err;
      } else if (resp.statusCode != 200) {
        errMsg = 'request ' + id + '[' +  url + '] statusCode:' + resp.statusCode;
      } else if (!buffer) {
        errMsg = 'request ' + id + '[' +  url + '] buffer empty';
      }
      if (errMsg) {
        pmx.notify({ event: 'request', error: errMsg });
        logger.error(errMsg);
        return res.redirect(doc.link);
      }
      header['Content-Type'] = resp.headers['content-type'];
      res.writeHead(200, header);
      if (header['Content-Type'].indexOf('text/html') == 0) { // 内容为 HTML 
        var $ = cheerio.load('<html>' + buffer + '</html>');
        var html = $('html');
        var body = $('body');
        if (!body.length) {
          body = $('<body>' + buffer + '</body>');
          html.html('');
          html.append(body);
        }
        body.html('<div class="seed_container">' + body.html() + '</div>');
        // body.prepend($('<div class="seed_header">由Seed为您优化并呈现</div>'));
        var head = $('head');
        if (!head.length) {
          head = $('<head></head>');
          html.prepend(head);
        }
        head.prepend($('<meta charset="utf-8">'));
        head.append($('<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">'));
        head.append($('<link rel="stylesheet" type="text/css" href="' + client + 'styles/originaltext.css">'));
        head.append($('<script src="' + client + 'scripts/zepto.js"></script>'));
        server($);
        head.append($('<script src="' + client + 'scripts/front.js"></script>'));
        buffer = html.html();
      }
      res.end(buffer);
      var millis = new Date() - start;
      logger.info('get source [%s] time:', id, millis);
      if (millis > TIMEOUT) {
        pmx.notify({ event: 'helloseed api timeout', error: 'get source [' +  id + '] timeout:' + millis });
      }
      if (USE_CACHE) { // 使用缓存
        memcached.set(key, buffer, 86400, function (err) { // 缓存一天
          if (err) {
            pmx.notify({ event: 'memcached', error: 'memcached set [' +  key + '] error:' + err });
            logger.fatal('memcached set [%s] error:', key, err);
            return;
          }
          logger.trace('cached:', key);
        });
      }
    });
  });
};

module.exports = sourceApi;