var TIMEOUT = 3000;

/**
 *  日志
 *  trace 跟踪
 *  debug 调试
 *  info  信息
 *  warn  警告
 *  error 错误
 *  fatal 致命
 */
var log4js = require('log4js'); // Port of Log4js to work with node
var logger = log4js.getLogger('html');
logger.setLevel('INFO');
var pmx = require('pmx'); // Keymetrics++ and PM2 adapter

var oss = require('../oss.js'); // 阿里云 OSS
var leancloud = require('../leancloud.js'); // leancloud rest api
var Article = require('../../../model/mongodb/Article.js')(); // 文章实体

var sources = [
  { selector: 'script[src]', attr: 'async', val: 'async' },
  { selector: 'link[rel="stylesheet"]', attr: 'href', tag: 'style' }
];

/**
 *  网页模式
 */
var uploadHtml = function (item, article, type, start, pushAgain) {
  if (item.htmlUrl && !item.force) {
    return;
  }
  var id = item.id;
  var link = item.link;
  article.getHtmls(sources, function (err, html) {
    var millis = new Date() - start;
    logger.info('read article\'s html %s[%s] time:', id, link, millis);
    if (millis > TIMEOUT) {
      pmx.notify({ event: 'helloseed mayi timeout', error: 'read article\'s html ' + id + '[' +  link + '] timeout:' + millis });
    }
    /**
     *  上传网页模式原文到 OSS
     */
    var key = 'html/' + id;
    logger.trace('html key:', key);
    logger.trace('html body:', html);
    logger.trace('html content-type:', type);
    oss.upload(type, html, key, function (err, data) {
      if (err) {
        pmx.notify({ event: 'oss put object', error: 'upload [' + key + '] error:' + err });
        logger.error('upload [%s] error:', key, err);
        return pushAgain(item);
      }
      logger.trace('upload [%s] success:', key, data);
      var millis = new Date() - start;
      logger.info('upload %s[%s] time:', key, link, millis);
      if (millis > TIMEOUT) {
        pmx.notify({ event: 'helloseed mayi timeout', error: 'upload ' + key + '[' +  link + '] timeout:' + millis });
      }
      /**
       *  更新网页模式原文信息到 article
       */
      logger.info('update article for html:', id);
      var updateData = {
        title: item.title,
        htmlUrl: oss.getUrl + '/' + key,
        countries: item.countries,
        updateDate: new Date()
      };
      Article.findByIdAndUpdate(id, { $set: updateData }, function (err, doc) {
        if (err) {
          pmx.notify({ event: 'update article', error: 'update [' + id + '] error:' + err });
          logger.fatal('update [%s] error:', id, err);
          return pushAgain(item);
        }
        logger.trace('update [%s] success:', id, doc);
        // 通知用户网页模式原文可阅读了
        var data = {
          id: id,
          url: link,
          htmlUrl: item.htmlUrl,
          linkUrl: item.linkUrl,
          alert: '您提交的链接网页模式解析完成：' + link
        };
        leancloud.push(item.userId, data, function (err, res) {
          if (err) {
            pmx.notify({ event: 'leancloud api', error: 'notification user[' + item.userId + '] error:' + err });
            logger.fatal('notification user[%s] error:', item.userId, err);
            return;
          }
          logger.trace('notification %s[%s] success:', id, link, res);
        });
        var millis = new Date() - start;
        logger.info('update html %s[%s] time:', id, link, millis);
        if (millis > TIMEOUT) {
          pmx.notify({ event: 'helloseed mayi timeout', error: 'update html ' + id + '[' +  link + '] timeout:' + millis });
        }
      });
    });
  });
};

module.exports = uploadHtml;