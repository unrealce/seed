var Memcached = require('memcached');
var memcached = new Memcached(process.env.Memcached_URL.split(','));
module.exports = memcached;