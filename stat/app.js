var express = require('express');

var app = express();
var bodyParser = require('body-parser');

app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/static'));
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/', function(req, res) {
  res.render('index.ejs');
});

app.post('/stat', require('./src/stat.js'));

app.all('*', function (req, res) {
  res.send('helloseed react running');
});

app.listen(3003);