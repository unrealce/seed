var TIMEOUT = 600; // the api timeout for the images (in milliseconds).

/**
 *  日志
 *  trace 跟踪
 *  debug 调试
 *  info  信息
 *  warn  警告
 *  error 错误
 *  fatal 致命
 */
var log4js = require('log4js'); // Port of Log4js to work with node
var logger = log4js.getLogger('imagse');
logger.setLevel('INFO');
var pmx = require('pmx'); // Keymetrics++ and PM2 adapter
/*
 *  encoding: null tells request that you want a buffer, not a string.
 *  A string gives you unusably garbled data, as the whole point of base64 is to encode binary.
 */
var request = require('request').defaults({ encoding: null }); // Simplified HTTP request client
var gm = require('gm'); // GraphicsMagick and ImageMagick for node.js

var USE_CACHE = require('../../../model/memcache/useCache.js')(); // 是否使用缓存
var memcached = require('../../../model/memcache/Memcached.js'); // 缓存服务
var Article = require('../../../model/mongodb/Article.js')(); // 文章实体
var ossUrl = require('../common/constants.js').ossUrl; // OSS 网址

var header = { 'Access-Control-Allow-Origin': '*' }; // 返回头部信息

var storeSizes = { // 设备尺寸
  android: { width: 280 },
  iPhone4: { width: 640 },
  iPhone6: { width: 750 },
  iPhone6p: { width: 1242 },
  weixin: { width: 64, height: 64 }
};

/**
 *  获取图片接口
流程：
  1.判断是否使用缓存
    否：跳到 3
    是：
      2.从缓存获取
        失败：
          3.查询数据库
            失败：
              4.获取默认图片
                失败：发送相应错误码
                成功：—— 返回默认图片数据
            成功：
              5.判断是否有mainUrl字段
                是：
                  6.从 OSS 获取
                    失败：跳到 7
                    成功：—— 返回 OSS 数据，跳到 9
                否：
                  7.获取标题图片
                    失败：跳到 4
                    成功：
                      8.GM 处理
                        失败：返回原图数据
                        成功：返回压缩数据
                          9.保存到缓存
                            失败：—— 记录日志，报警
                            成功：—— 记录日志
        成功：—— 返回缓存数据
 */
var imageApi = function (req, res) {
  var id = req.params.id; // 文章 ID
  var store = req.query.store; // 设备
  var width = parseInt(req.query.width); // 宽
  var height = parseInt(req.query.height); // 高
  logger.debug('id:', id);
  logger.debug('store:', store);
  logger.debug('width:', width);
  logger.debug('height:', height);
  if (!store && !width && !height) {
    store = 'android';
  }
  if (store) {
    if (!width) {
      width = storeSizes[store].width;
    }
    if (!height) {
      height = storeSizes[store].height;
    }
  }
  /**
   *  构造 OSS Key
   */
  var key = 'images/' + id + '.';
  if (store) {
    key += store;
  } else {
    key += width + '.' + height;
  }
  /**
   *  1.判断是否使用缓存
   */
  USE_CACHE ? getFromCache(res, key, id, width, height) : getFromDB(res, key, id, width, height);
}

/**
 *  2.从缓存获取
 */
var getFromCache  = function (res, key, id, width, height) {
  var start = new Date();
  memcached.get(key, function (err, data) { // 查询缓存
    if (err) {
      pmx.notify({ event: 'memcached', error: 'memcached get [' +  key + '] error:' + err });
      logger.error('memcached get [%s] error:', key, err);
      return getFromDB(res, key, id, width, height); // 从OSS获取
    }
    if (!data) {
      pmx.notify({ event: 'memcached', error: 'memcached get [' +  key + '] empty' });
      logger.error('memcached get [%s] empty', key);
      return getFromDB(res, key, id, width, height); // 从OSS获取
    }
    logger.info('memcached key:', key);
    logger.debug('memcached data:', data);
    res.writeHead(200, header);
    res.end(data);
    var millis = new Date() - start;
    logger.info('memcached get [%s] time:', key, millis);
    if (millis > TIMEOUT) {
      pmx.notify({ event: 'helloseed api timeout', error: 'memcached get [' +  key + '] timeout:' + millis });
    }
    logger.info('memcached get [%s] time:', key, millis);
  });
};

/**
 *  3.查询数据库
 */
var getFromDB = function (res, key, id, width, height) {
  var start = new Date();
  Article.findOne({ _id: id }, 'image.url mainUrl', function (err, doc) {
    if (err) {
      pmx.notify({ event: 'find article', error: 'find article[' + id + '] error:' + err });
      logger.fatal('find article[%s] error:', id, err);
      return getDefaultImg(res); // 获取默认图片
    }
    if (!doc || !doc.image || !doc.image.url) {
      pmx.notify({ event: 'find article', error: 'find article[' + id + '] empty' });
      logger.fatal('find article[%s] empty', id);
      return getDefaultImg(res); // 获取默认图片
    }
    logger.info('item:', doc);
    var millis = new Date() - start;
    logger.info('get item [%s] time:', id, millis);
    if (millis > TIMEOUT) {
      pmx.notify({ event: 'helloseed api timeout', error: 'get item [' + id + '] timeout:' + millis });
    }
    /**
     *  5.判断是否有mainUrl字段
     */
    if (doc.mainUrl) {
      getFromOSS(res, key, id, width, height, doc); // 从OSS获取
    } else {
      getTilieImg(res, key, id, width, height, doc.image.url); // 获取标题图片
    }
  });
};

/**
 *  4.获取默认图片
 */
var getDefaultImg = function (res) {
  var start = new Date();
  request(ossUrl + '/default.jpg', function (err, resp, body) {
    if (err) {
      pmx.notify({ event: 'request', error: 'request default image error:' + err });
      logger.error('request default image error:', err);
      return res.sendStatus(500);
    }
    if (resp.statusCode != 200) {
      pmx.notify({ event: 'request', error: 'request default image statusCode:' + resp.statusCode });
      logger.error('request default image statusCode:', resp.statusCode);
      return res.sendStatus(resp.statusCode);
    }
    if (!body) {
      pmx.notify({ event: 'request', error: 'request default image empty' });
      logger.error('request default image empty');
      return res.sendStatus(404);
    }
    header['Content-Type'] = resp.headers['content-type'];
    res.writeHead(200, header);
    res.end(body);
    var millis = new Date() - start;
    logger.info('default image get time:', millis);
    if (millis > TIMEOUT) {
      pmx.notify({ event: 'helloseed api timeout', error: 'default get timeout:' + millis });
    }
  });
};

/**
 *  6.从 OSS 获取
 */
var getFromOSS  = function (res, key, id, width, height, doc) {
  var start = new Date();
  var url = doc.mainUrl.replace('main/' + id, key); // OSS 地址
  request(url, function (err, resp, body) {
    var errMsg; // 错误信息
    if (err) {
      errMsg = 'request ' + id + '[' +  url + '] error:' + err;
    } else if (resp.statusCode != 200) {
      errMsg = 'request ' + id + '[' +  url + '] statusCode:' + resp.statusCode;
    } else if (!body) {
      errMsg = 'request ' + id + '[' +  url + '] body empty';
    }
    if (errMsg) {
      pmx.notify({ event: 'request', error: errMsg });
      logger.error(errMsg);
      return getTilieImg(res, key, id, width, height, doc.image.url); // 获取标题图片
    }
    logger.info('image:', url);
    header['Content-Type'] = resp.headers['content-type'];
    res.writeHead(200, header);
    res.end(body);
    var millis = new Date() - start;
    logger.info('oss get [%s] time:', url, millis);
    if (millis > TIMEOUT) {
      pmx.notify({ event: 'helloseed api timeout', error: 'oss get ' + '[' + url + '] timeout:' + millis });
    }
    setCache(key, body); // 保存到缓存
  });
};

/**
 *  7.获取标题图片
 */
var getTilieImg = function (res, key, id, width, height, url, encode) {
  request(url, function (err, resp, body) {
    var errMsg; // 错误信息
    if (err) {
      errMsg = 'request ' + id + '[' +  url + '] error:' + err;
    } else if (resp.statusCode != 200) {
      errMsg = 'request ' + id + '[' +  url + '] statusCode:' + resp.statusCode;
    } else if (!body) {
      errMsg = 'request ' + id + '[' +  url + '] body empty';
    }
    if (errMsg) {
      pmx.notify({ event: 'request', error: errMsg });
      logger.error(errMsg);
      return encode ? getDefaultImg(res) : getTilieImg(res, key, id, width, height, encodeURI(url), true);
    }
    logger.info('image:', url);
    gmProcess(res, key, width, height, body); // GM处理
  });
};

/**
 *  8.GM 处理
 */
var gmProcess = function (res, key, width, height, body) {
  var start = new Date();
  var file = gm(body);
  file.compress('JPEG'); // JPEG 压缩 body
  file.resize(width, height); // 缩放
  file.quality(40); // 质量压缩
  file.toBuffer('JPEG', function (err, buffer) {
    if (err) {
      pmx.notify({ event: 'gm', error: key + ' gm to buffer error:' + err });
      logger.error('%s gm to buffer error:', key, err);
      /**
       *  gm to buffer 失败传原图
       */
      buffer = body;
    }
    res.writeHead(200, header);
    res.end(buffer);
    var millis = new Date() - start;
    logger.info('title image get [%s] time:', key, millis);
    if (millis > TIMEOUT) {
      pmx.notify({ event: 'helloseed api timeout', error: 'title image get ' + '[' + key + '] timeout:' + millis });
    }
    logger.trace('upload jpg:', key);
    setCache(key, buffer); // 保存到缓存
  });
};

/**
 *  9.保存到缓存
 */
var setCache = function (key, value) {
  if (USE_CACHE) { // 使用缓存
    memcached.set(key, value, 86400, function (err) { // 缓存一天
      if (err) {
        pmx.notify({ event: 'memcached', error: 'memcached set [' +  key + '] error:' + err });
        logger.fatal('memcached set [%s] error:', key, err);
        return;
      }
      logger.trace('cached:', key);
    });
  }
};

module.exports = imageApi;