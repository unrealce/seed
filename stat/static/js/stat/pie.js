var createPie = function(data) {
  $('#pie').kendoChart({
    title: { text: "SourceType Rate" },
    legend: { position: "top" },
    seriesDefaults: {
      type: 'pie',
      labels: {
        template: '${ category } - ${ value }%',
        visible: true
      }
    },
    series: [{ data: data.series }],
    tooltip: {
      template: '${ category } - ${ value }%',
      visible: true
    }
  });
};