var LC_URL = 'https://leancloud.cn/1.1/';

var fetchUrl = require('fetch').fetchUrl;

var options = {
  headers: {
    'Content-Type': 'application/json',
    'X-LC-Id': 'm7u3qeqxmoxu2xxn3ainynms7ty0z01hl5kkt7tdn4c9lngl',
    'X-LC-Key': 'fk32q34idm2iwmvs1qewa6ynoquousjmr0xoeepdoczrrtgv'
  },
  method: 'POST'
};

module.exports.push = function (userId, data, callback) {
  if (!userId) {
    return callback(null, 'user\'s id is empty');
  }
  var formData = {
    data: data,
    prod: process.env.NODE_ENV == 'prod' ? 'prod' : 'dev',
    where: {
      owner: { __type: 'Pointer', className: '_User', objectId: userId }
    }
  };
  options.payload = JSON.stringify(formData);
  fetchUrl(LC_URL + 'push', options, function(err, res, buf) {
    if (err) {
      return callback(err);
    }
    if (res.status != 200) {
      return callback(new Error('status code: ' + res.status));
    }
    return callback(null, buf.toString());
  });
};