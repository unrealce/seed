var mongoose = require('./Mongoose')();
var CronHistory = mongoose.model(
  'cronHistory',
  {
    name: String,
    status String,
    startedAt: Date,
    finishedAt: Date
  }
);
module.exports = CronHistory;