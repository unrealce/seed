/**
 *  日志
 *  trace 跟踪
 *  debug 调试
 *  info  信息
 *  warn  警告
 *  error 错误
 *  fatal 致命
 */
var log4js = require('log4js'); // Port of Log4js to work with node
var logger = log4js.getLogger('item');
logger.setLevel('INFO');
var pmx = require('pmx'); // Keymetrics++ and PM2 adapter
var request = require('request'); // Simplified HTTP request client
var parseString = require('xml2js').parseString; // Simple XML to JavaScript object converter
var parseDomain = require('parse-domain'); // Splits an url into sub-domain, domain and top-level-domain

var sourceUrls = require('../common/sourceUrls.js'); // 抓取源
var options = require('../common/options.js'); // 浏览器头部信息
var memcached = require('../../../model/memcache/Memcached.js'); // 缓存服务
var Article = require('../../../model/mongodb/Article.js')(); // 文章实体

var getSourceUrls = require('../third_party/leancloud/getSourceUrls.js'); // leancloud

var client = process.env.SEED_CLIENT; // 接口地址

/**
 *  下载文章任务
 */
var itemTask = function () {
  var start = new Date();
  logger.info('item task start');
  memcached.get('sourceUrls', function (err, data) { // 查询缓存
    logger.info('memcached get time:', new Date() - start);
    if (err) {
      pmx.notify({ event: 'memcached', error: 'memcached get [sourceUrls] error:' + err });
      logger.error('memcached get [sourceUrls] error:', err);
    } else {
      logger.debug('memcached data:', data);
      if (data) { // 缓存数据
        return downloadUrls(data, start); // 下载 URL
      }
    }
    getSourceUrls(function (err, data) {
      if (err) {
        pmx.notify({ event: 'getSourceUrls', error: 'getSourceUrls error:' + err });
        logger.error('getSourceUrls error:', err);
        return;
      }
      if (!data) {
        pmx.notify({ event: 'sourceUrls', error: 'sourceUrls empty' });
        logger.warn('sourceUrls empty');
        return;
      }
      data.kickstarter = sourceUrls.kickstarter; // 私心
      memcached.set('sourceUrls', data, 7200, function (err) { // 缓存2小时
        if (err) {
          pmx.notify({ event: 'memcached', error: 'memcached set [sourceUrls] error:' + err });
          logger.fatal('memcached set [sourceUrls] error:', err);
          return;
        }
        logger.trace('cached:', 'sourceUrls');
      });
      return downloadUrls(data, start); // 下载 URL
    });
  });
  logger.info('item task end');
};

/**
 *  下载 URL
 */
var downloadUrls = function (sourceUrls, start) {
  for (var type in sourceUrls) {
    sourceUrls[type].forEach(function (url) {
      logger.trace('download %s:', type, url);
      downloadUrl(type, url, start); // 下载文章
    });
  }
};

/**
 *  下载文章
 */
var downloadUrl = function (type, url, start) {
  request(url.url + '.rss', options, function (err, resp, body) {
    logger.info('request [%s/%s] time:', url.name, url.url, new Date() - start);
    if (err) {
      pmx.notify({ event: 'request', error: 'request [' + url.name + '/' + url.url + '] error:' + err });
      logger.error('request [%s/%s] error:', url.name, url.url, err);
      return;
    }
    if (resp.statusCode != 200) {
      pmx.notify({ event: 'request', error: 'request [' + url.name + '/' + url.url + '] statusCode:' + resp.statusCode });
      logger.error('request [%s/%s] statusCode:', url.name, url.url, resp.statusCode);
      return;
    }
    if (!body) {
      pmx.notify({ event: 'request', error: 'request [' + url.name + '/' + url.url + '] body empty' });
      logger.error('request [%s/%s] body empty', url.name, url.url);
      return;
    }
    logger.info('%s:', type, url);
    logger.debug('url body:', body);
    parseString(body, { trim: true }, function (err, result) {
      if (err) {
        pmx.notify({ event: 'xml2js', error: url.name + ' parseString [' +  url.url + '] error:' + err });
        logger.error('%s parseString [%s] error:', url.name, url.url, err);
        return;
      }
      /**
       *  解析 article
       */
      logger.debug('result:', result);
      var channels = result.rss.channel;
      for (var i in channels) {
        var channel = channels[i];
        var items = channel.item;
        if (!items) {
          pmx.notify({ event: 'warn', error: 'items is empty from ' + type });
          logger.warn('items is empty:', type);
          continue;
        }
        for (var j = items.length - 1; j > -1; j--) { // 倒序遍历 article
          var item = items[j];
          logger.trace('item:', item);
          var title = item['title'][0];
          if (/\w+ on Flipboard$/.test(title)) {
            pmx.notify({ event: 'warn', error: 'exclude《' + title + '》from ' + type + ':' + url });
            logger.warn('exclude《%s》from %s:', title, type, url);
            continue;
          }
          saveItem(type, url, item, start); // 保存文章
        }
      }
    });
  });
};

/**
 *  保存文章
 */
var saveItem = function (type, url, item, start) {
  var link = item['link'][0];
  Article.findOne({ link: link, sourceName: url.name }, 'sourceType link linkUrl htmlUrl', function (err, doc) {
    logger.info('find article [%s] time:', link, new Date() - start);
    if (err) {
      pmx.notify({ event: 'find article', error: 'find link[' + link + '] error:' + err });
      logger.fatal('find link[%s] error:', link, err);
      return;
    }
    if (doc) {
      logger.debug('finded article:', doc);
      logger.info('finded link:', link);
      if (!doc.sourceType) {
        /**
         *  更新 source 信息到 article
         */
        Article.findByIdAndUpdate(
          doc._id,
          {
            $set: {
              guid: item.guid._, // flipboard's id
              sourceUrl: url.url, // 源地址
              sourceName: url.name, // 源名称
              sourceType: type, // 源类型
              updateDate: new Date()
            }
          },
          function (err, doc) {
            logger.info('update article [%s] time:', doc._id, new Date() - start);
            if (err) {
              pmx.notify({ event: 'update article', error: 'update [' + doc._id + '] error:' + err });
              logger.fatal('update [%s] error:', doc._id, err);
              return;
            }
            logger.trace('update [%s] success:', doc._id, doc);
          }
        );
      }
      if (!doc.htmlUrl || !doc.linkUrl) {
        parseUrl(doc.link, start); // 解析文章
      }
      return;
    }
    logger.info('not find link:', link);
    var article = new Article(); // 文章实例
    for (var i in item) {
      if (item[i].length == 1) { // array -> object
        item[i] = item[i][0];
      }
      article[i] = item[i];
    }
    logger.trace('orig article:', article);
    /**
     *  处理 guid
     */
    var guid = item.guid;
    article.guid = guid._;
    /**
     *  处理媒体内容 media:content
     */
    if (item['media:content']) {
      var media = item['media:content'].$;
      logger.trace('media:', media);
      article.image = {
        url: media.url,
        width: media.width,
        height: media.height,
        imgType: media.type
      };
    }
    /**
     *  处理短文
     */
    if (article.description) {
      article.descriptionOrig = article.description; // 原短文
      article.description = article.description.replace(/<\S+>/g, ' '); // 去除 HTML 标签(替换为空格)
    }
    /**
     *  解析 domain
     */
    var pd = parseDomain(link.replace('@', ''));
    if (!pd) {
      pmx.notify({ event: 'warn', error: 'parse [' + link + '] domain fail' });
      logger.warn('parse domain fail:', link);
      return;
    }
    article.linkSubdomain = pd.subdomain;
    article.linkDomain = pd.domain;
    article.linkTld = pd.tld;
    /**
     *  初始化阅读数
     */
    article.readCount = 0; // parseInt(Math.random() * 1000);
    article.realReadCount = 0;
    /**
     *  插入基础信息
     */
    article.sourceUrl = url.url; // 源地址
    article.sourceName = url.name; // 源名称
    article.sourceType = type; // 源类型
    article.createDate = new Date(); // 创建时间
    /**
     *  插入 article
     */
    logger.debug('article:', article);
    article.save(function (err, results) {
      logger.info('save article [%s] time:', article._id, new Date() - start);
      if (err) {
        pmx.notify({ event: 'save article', error: 'save [' + article + '] error:' + err });
        logger.fatal('save [%s] error:', article, err);
        return;
      }
      logger.trace('saveed:', results);
      parseUrl(article.link, start); // 解析文章
    });
  });
};

/**
 *  解析文章
 */
var parseUrl = function (url, start) {
  logger.info('parse url:', url);
  request.post(client + 'parse', { form: { url: url } }, function (err, resp, body) {
    logger.info('request post [%s] time:', url, new Date() - start);
    if (err) {
      pmx.notify({ event: 'request', error: 'request [' + url + '] error:' + err });
      logger.error('request [%s] error:', url, err);
      return;
    }
    if (resp.statusCode != 200) {
      pmx.notify({ event: 'request', error: 'request [' + url + '] statusCode:' + resp.statusCode });
      logger.error('request [%s] statusCode:', url, resp.statusCode);
      return;
    }
    if (!body) {
      pmx.notify({ event: 'request', error: 'request [' + url + '] body empty' });
      logger.error('request [%s] body empty', url);
      return;
    }
    logger.debug('url body:', body);
    if(body != 'success') {
      parseUrl(url, start);
    }
  });
};

module.exports = itemTask;