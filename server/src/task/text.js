/**
 *  日志
 *  trace 跟踪
 *  debug 调试
 *  info  信息
 *  warn  警告
 *  error 错误
 *  fatal 致命
 */
var log4js = require('log4js'); // Port of Log4js to work with node
var logger = log4js.getLogger('text');
logger.setLevel('INFO');
var pmx = require('pmx'); // Keymetrics++ and PM2 adapter
var request = require('request'); // Simplified HTTP request client

var options = require('../common/options.js'); // 浏览器头部信息
var Article = require('../../../model/mongodb/Article.js')(); // 文章实体

var sourceNames = ['editors-pick', 'the-techy', 'the-trendy', 'the-pretty', 'entertainment'];

/**
 *  下载 caption 任务
 */
var textTask = function () {
  var start = new Date();
  logger.info('text task start');
  var yesterday = new Date();
  yesterday.setDate(yesterday.getDate() - 1);
  Article.find({ createDate: { $gt: yesterday }, sourceName: { $in: sourceNames } }, 'guid caption', function (err, items) {
    logger.info('find articles time:', new Date() - start);
    if (err) {
      pmx.notify({ event: 'find article', error: 'find text articles error:' + err });
      logger.fatal('find text articles error:', err);
      return;
    }
    if (!items) {
      pmx.notify({ event: 'warn', error: 'items is empty' });
      logger.warn('items is empty');
      return;
    }
    logger.info('find ' + items.length + ' text articles');
    logger.debug('articles\' items:', items);
    items.forEach(function (item) {
      logger.debug('item:', item);
      if (!item.caption) {
        downloadText(item, start);
      }
    });
  });
  logger.info('text task end');
};

/**
 *  下载 caption
 */
var downloadText = function (item, start) {
  var url = 'https://flipboard.com/api/social/commentary?oid=' + item.guid;
  request(url, options, function (err, resp, body) {
    logger.info('request [%s] time:', url, new Date() - start);
    if (err) {
      pmx.notify({ event: 'request', error: 'request [' +  url + '] error:' + err });
      logger.error('request [%s] error:', url, err);
      return;
    }
    if (resp.statusCode != 200) {
      pmx.notify({ event: 'request', error: 'request [' +  url + '] statusCode:' + resp.statusCode });
      logger.error('request [%s] statusCode:', url, resp.statusCode);
      return;
    }
    if (!body) {
      pmx.notify({ event: 'request', error: 'request [' +  url + '] body empty' });
      logger.error('request [%s] body empty', url);
      return;
    }
    logger.debug('body:', body);
    var commentary = JSON.parse(body).items[0].commentary;
    logger.trace('commentary:', commentary);
    if (commentary && commentary.length) {
      var text = commentary[0].text;
      logger.trace('text:', text);
      /**
       *  更新 caption 信息到 article
       */
      Article.findByIdAndUpdate(
        item._id,
        {
          $set: {
            caption: text,
            updateDate: new Date()
          }
        },
        function (err, doc) {
          logger.info('update article [%s] time:', item._id, new Date() - start);
          if (err) {
            pmx.notify({ event: 'update article', error: 'update [' + item._id + '] error:' + err });
            logger.fatal('update [%s] error:', item._id, err);
            return;
          }
          logger.trace('update [%s] success:', item._id, doc);
        }
      );
    }
  });
};

module.exports = textTask;