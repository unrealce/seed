var ArticleSchema = {
  title: String, // 接口返回
  link: String, // 
  guid: String, // 
  pubDate: Date, // 接口返回
  author: String, // 
  description: String, // 接口返回
  descriptionOrig: String, // 
  image: {
    _id: String, // 接口返回
    url: String, // 
    ossUrl: String, // 
    width: Number, // 接口返回
    height: Number, // 接口返回
    imgType: String, // 
    imgWidth: Number, // 接口返回
    imgHeight: Number, // 接口返回
  },
  images: [{
    url: String, // 
    ossUrl: String, // 
    width: Number, // 
    height: Number, // 
    imgType: String, // 
  }],
  sourceUrl: String, // 
  sourceName: String, // 
  sourceType: String, // 接口返回
  createDate: Date, // 
  updateDate: Date, // 
  htmlUrl: String, // 接口返回
  mainUrl: String, // 
  linkUrl: String, // 接口返回
  linkSubdomain: String, // 
  linkDomain: String, // 接口返回
  linkTld: String, // 
  countries: [String], // 
  caption: String, // 接口返回
  content: String, // 
  readCount: Number, // 接口返回
  realReadCount: Number // 
};
module.exports = ArticleSchema;