var constants = {
  client: process.env.SEED_CLIENT,
  ossUrl: 'http://' + process.env.OSS_BUCKET + '.oss-cn-beijing.aliyuncs.com/',
  projectionDefault: 'title sourceType sourceName pubDate author description image._id image.width image.height image.imgWidth image.imgHeight' // 默认返回字段
};
constants.projectionDefault += ' linkDomain linkTld htmlUrl linkUrl caption readCount createDate'; // 附加字段
// if (process.env.NODE_ENV != 'prod') {
  constants.projectionDefault += ' content'; // 异步返回阅读模式原文内容
// }
module.exports = constants;