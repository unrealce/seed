var fetch = require('fetch'); // Fetch URL contents
var fetchUrl = fetch.fetchUrl;

var hostnameParse = {
  'www.businessinsider.com': 'www.businessinsider.com.au'
};

var maybeImgsAttr = [ // 可能是图片的属性
  'data-lightbox-image-url',
  'rel:bf_image_src',
  'data-src-medium',
  'data-src-small',
  'data-asset-url',
  'data-original',
  'data-lazy-src',
  'data-imagesrc',
  'data-srcset',
  'data-medsrc',
  'data-smsrc',
  'data-lgsrc',
  'data-cfsrc',
  'data-src',
  'load-src',
  'href',
  'src',
  'srcset'
];

var nodesToRemove = [ // 需要删除的标签
  'svg', // svg
  'meta', // 元数据
  'link', // 样式链接
  'style', // 样式
  'video', // 视频
  'input', // 输入框
  'button', // 按钮
  'object', // 对象
  'canvas', // 画布
  'textarea', // 文本框
  'iframe:not([src])', // 内嵌模块
  'div#pinned', // engadget.com
  'div#respond', // thesartorialist.com
  'div#sidebar', // demilked.com
  'div#macro-fc20', // fastcodesign.com
  'div#page-title', // 页面标题
  'div#post-river', // businessinsider.com
  'div#tags h3.msg', // abduzeedo.com
  'div#gdgt-wrapper', // engadget.com
  'div#disqus_thread', // primermagazine.com
  'div#sidebar-wrapper', // fastcompany.com
  'div#article-most-popular', // theatlantic.com
  'div.fb-box', // facebook
  'div.loading', // 加载中
  'div.rweb-ad', // washingtonpost.com
  'div.articleSM', // 分享模块
  'div.m-linkset', // theverge.com
  'div.read-more', // huffingtonpost.com
  'div.tag-cloud', // huffingtonpost.com
  'div.fout_guard', // fout 守卫
  'div.navigation', // macrumors.com
  'div.buttonGroup', // 按钮组
  'div.bn-mo-tests', // 测试模块
  'div.itemRelated', // 相关文章
  'div.ks-see-also', // 也可以看看
  'div.most-shared', // 大多数分享
  'div.PopularPosts', // arkinspace.com
  'div.article-extra', // techcrunch.com
  'div.comments-main', // 评论
  'div.control-panel', // 控制面板
  'div.give-feedback', // macrumors.com
  'div.list-comments', // thesartorialist.com
  'div.related-posts', // hypebeast.com
  'div.like-post-left', // demilked.com
  'div.postActionsBar', // medium.com
  'div.ssw-section-ad', // cntraveler.com
  'div.article-actions', // food52.com
  'div.cnt-page-header', // cntraveler.com
  'div.content__header', // theguardian.com
  'div.panes-container', // huffingtonpost.com
  'div.fyre-post-button', // timeout.com
  'div.m-entry__sidebar', // theverge.com
  'div.post-dropdown-ct', // gawker.com
  'div.recirc-accordion', // techcrunch.com
  'div.bloc-related-post', // thesartorialist.com
  'div.boilerplate-after', // venturebeat.com
  'div.slideshow-control', // dezeen.com
  'div.archive-pagination', // nomadicmatt.com
  'div.crunchbase-cluster', // techcrunch.com
  'div.dropdown-container', // engadget.com
  'div.expand-mobile-fold', // demilked.com
  'div.newsletters-signup', // techcrunch.com
  'div.recommended-widget', // vice.com
  'div.fyre-follow-button', // timeout.com
  'div.fyre-comment-stream', // timeout.com
  'div.fyre-format-toolbar', // timeout.com
  'div.js-reset-video-once', // kickstarter.com
  'div.article-copy--footer', // 文章脚注
  'div.article-list-wrapper', // uncrate.com
  'div.inline-share-wrapper', // nytimes.com
  'div.rail-collection-main', // rail 广告
  'div.gallery-overlay-outter', // 外部相册
  'div.article-related-coverage', // nytimes.com
  'div.js-top-android-news-swarm', // buzzfeed.com
  'div.recommended-photo-article', // theatlantic.com
  'div.forces-video-controls_hide', // kickstarter.com
  'div.article-cover-extra-wrapper', // theatlantic.com
  'div.slide-show--slide__template', // 幻灯片模板
  'div.sponsored-post-list-wrapper', // uncrate.com
  'div.module-top-pathing--container', // esquire.com
  'div.slide-show--slide__template--video', // 幻灯片模板-视频
  'div[role="complementary"]', // thesartorialist.com
  'a.add-post', // demilked.com
  'a.more-link', // nomadicmatt.com
  'a.button-link', // washingtonpost.com
  'aside#hot-buzz-stats', // buzzfeed.com
  'aside#reactions-related', // buzzfeed.com
  'aside.sidebar', // core77.com
  'aside.app_promo', // buzzfeed.com
  'aside.side_content', // timeout.com
  'aside.cookie-notice', // buzzfeed.com
  'article.unloaded', // washingtonpost.com
  'ins.adsbygoogle', // google 广告
  'ol.comment-list', // nomadicmatt.com
  'ul#user-actions-dropdown', // engadget.com
  'ul#related_feed', // 相关 feed
  'ul.fyre-box-list', // timeout.com
  'ul.golden_layout', // core77.com
  'ul.commentlist', // smittenkitchen.com
  'p.postmetadata', // smittenkitchen.com
  'p.see-all', // engadget.com
  'span.hide', // 隐藏内容
  'section#single_bottom_posts', // primermagazine.com
  'section#video-categories', // mashable.com
  'section.content-sharing', // food52.com
  'section.extra-content', // huffingtonpost.com
  'section.crunchreport', // techcrunch.com
  '.robots-nocontent', // 非内容模块
  'header#site-header-container', // theatlantic.com
  'header.article-header div.tags', // techcrunch.com
  'header.header-context-news', // techcrunch.com
  'header.entry-headline', // 头部信息
  'header.m-header', // theverge.com
  'header[role="banner"]', // engadget.com
  'footer,#footer,.footer' // 尾部信息
];

var noChdToRemove = [ // 没有子节点的时候需要删除的标签
  'div',
  'li'
];

module.exports = function (domain) {
  var options = {
    headers: { 'User-Agent': 'Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.20 (KHTML, like Gecko) Mobile/7B298g' },
    hostnameParse: hostnameParse,
    maybeImgsAttr: maybeImgsAttr,
    nodesToRemove: nodesToRemove,
    noChdToRemove: noChdToRemove
  };
  switch (domain) {
    // use default User-Agent
    case 'businessinsider':
    case 'kickstarter':
    case 'indiegogo':
    case 'cnn':
      delete options.headers;
      break;
    // urlprocess
    case 'instagram':
      options.urlprocess = function (url) {
        if (/^https:\/\/instagram\.com\/p\/[\w-]+\/$/.test(url)) {
          url += 'embed';
        }
        return url;
      };
      break;
    case 'youtube':
      options.urlprocess = function (url) {
        return url.replace('watch?v=', 'embed/');
      };
      break;
    // preprocess
    case 'buzzfeed':
      options.preprocess = function ($, options) {
        $('div.video-embed').each(function (index, element) {
          var node = $(element);
          var url = node.attr('data-src');
          if (url) {
            var iframe = '<iframe src="http:' + url + '"></iframe>';
            node.append(iframe);
          }
        });
      };
      options.postprocess = function (content, $) {
        content.find('blockquote.bf-tweet').addClass('twitter-tweet');
        if (content.find('blockquote.twitter-tweet').length) {
          content.prepend($('<script async src="http://platform.twitter.com/widgets.js" charset="utf-8"></script>'));
        }
      };
      break;
    // postprocess
    case 'businessoffashion':
      options.postprocess = function (content, $) {
        var header = $('div.article-header-fullscreen,div.article-header-collection');
        if (!header || !header.length) {
          return;
        }
        header.find('p.sans-serif').remove();
        content.prepend(header);
      };
      break;
    case 'fastcocreate':
    case 'fastcoexist':
    case 'fastcompany':
      options.postprocess = function (content, $) {
        var header = $('section#page-jumbotron');
        if (!header || !header.length) {
          return;
        }
        content.prepend(header);
      };
      break;
    case 'uncrate':
      options.postprocess = function (content, $) {
        var header = $('div.article-single div.image-wrapper');
        if (!header || !header.length) {
          return;
        }
        content.prepend(header);
      };
      break;
    // cookies
    case 'nytimes':
      options.asyncprocess = function (url, options, callback) {
        fetchUrl(url, function(err, res, buf) {
          if (err) {
            return callback(err);
          }
          try {
            var NYT_S = res.cookieJar.cookies['NYT-S'][0].value;
            var cookies = new fetch.CookieJar(); // for sharing cookies between requests
            cookies.setCookie('NYT-S=' + NYT_S);
            options.cookieJar = cookies;
          } catch (e) {
          }
          callback(null);
        });
      };
      break;
  }
  return options;
};