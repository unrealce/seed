$(document).ready(function() {
  var setChange = function() {
    var endDate = end.value();
    var startDate = start.value();
    start.max(endDate ? new Date(endDate) : new Date());
    end.min(startDate ? new Date(startDate) : new Date());
  }
  var start = $('#start').kendoDatePicker({
    change: setChange
  }).data('kendoDatePicker');
  var end = $('#end').kendoDatePicker({
    change: setChange
  }).data('kendoDatePicker');
  var now = new Date();
  start.value(now);
  end.value(now);
  start.max(now);
  end.min(now);
  end.max(now);
  $('select').kendoDropDownList();
  $('#query').click(function () {
    $.post('stat', { start: start.value(), end: end.value(), unit: $('#unit').val(), field: $('#field').val() }, function(data) {
      createArea(data.area);
      createPie(data.pie);
    });
  }).click();
});