'use strict';

/*map dom elements to json*/
function mapDOM(element) {
    var treeObject = {};
    function treeHTML(element, object) {
        //console.log(element);
        object["type"] = element.name;
        var nodeList = element.childNodes;
        if (nodeList != null) {
            if (nodeList.length) {
                object["content"] = [];
                for (var i = 0; i < nodeList.length; i++) {
                    if (nodeList[i].nodeType == 3) {
                        object["content"].push(nodeList[i].nodeValue);
                    } else {
                        object["content"].push({});
                        treeHTML(nodeList[i], object["content"][object["content"].length -1]);
                    }
                }
            }
        }
        if (element.attribs != null) {
            //if (element.attribs.length) {
            object["attributes"] = {};
            //console.log(element.attribs);
            for( var key in element.attribs ){
                //console.log(key,element.attribs[key]);
                object["attributes"][key] = element.attribs[key];
            }
            //}
        }
    }
    treeHTML(element, treeObject);
    return treeObject.content;
}

/*feature detect*/
var isInvisible = function(elem){
    var display = elem.style.display;
    if(display.match(/none/)) return true;
    return false;
}

/*layout fixes*/
var fixRecursive = function(element, tag) {
    var elem = $(element);
    var pare = elem.parent();
    var css  = elem.attr('css');
    if ( pare && pare.is(tag) && pare.children().length == 1 ) {
        elem.replaceWith(elem.html());
        if(css != null) pare.addClass(css);
    }
}

/*var upperImage = function(elem){
    var pare = elem.parent();
    if(!pare.hasClass('seed_container')){
        if(pare.children().length  == 1){
            pare.replaceWith(pare.html());
        }
    }
}*/

var reformatJsonToHTML = function(nodes,html,tag){
    if(nodes == undefined) return html;
    var i,len, node, type, fix, str, classes='', attr;
    //console.log(nodes);
    for( i=0,len=nodes.length; i<len; i++ ){
        node = nodes[i];
        type = node.type;
        fix  = node.fix;
        //console.log(node,type,fix);
        if(fix != undefined && fix != 2){
            if(type == undefined){
                str = node;
            }else{
                str = '<'+type+'>'+node.content[0]+'</'+type+'>';
            }
            if(fix == 0){
                html += '<'+tag+'>'+str+'</'+tag+'>';
            }else if(fix == 1){
                html += '<'+tag+'>'+str;
            }else if(fix == 3){
                html += str+'</'+tag+'>';
            }
            //console.log(html);
        }else if(type != undefined){
            attr = node.attributes;classes = '';
            if(attr != undefined){
                for( var key in attr )
                    classes += key+"='"+attr[key]+"' ";
            }
            if(/BR/i.test(type)){
                html += '<br>'+reformatJsonToHTML(node.content,''); //node.content[0];
            }else if(/IMG|IMAGE/i.test(type)){
                html += '<img src="'+attr.src+'" style='+attr.style+' '+classes+'/>';
            }else if(/IFRAME/i.test(type)){
                html += '<iframe src="'+attr.src+'" '+classes+'></iframe>';
            }else if(type != '#comment'){
                //console.log(type,node.content);
                if( node.content == undefined || (typeof node.content == 'string' && node.content.trim().length <= 0) ){
                    //console.log(type,node.content);
                    continue;
                }
                //console.log('passed');
                html += '<'+type+' '+classes+'>';
                html += reformatJsonToHTML(node.content,'');
                html += '</'+type+'>';
            }
            //console.log(html);
        }else if(typeof node == 'string' && node.trim().length > 0){
            html += node;
            //console.log(html);
        }
    }
    return html;
}

var wrapRawtext = function(elem,tag){
    var node         = mapDOM(elem);
    var hasrawstring = false, i = 0, len, up;
    var html         = elem.innerHTML;
    var updates      = [], cnode, nnode, snode = [];
    
    for(len = node.length; i < len; i++){
        cnode = node[i];
        if(typeof cnode == 'string'){
            if(cnode.trim().length > 0)
                snode.push(cnode);
        }/*else if(cnode.type == 'undefined'){
            if( (cnode.content.length == 1) && 
                (typeof cnode.content[0] == 'string') && 
                (cnode.content[0].trim().length > 0) )
                snode.push(cnode);
            else if(cnode.content.length > 1)
                snode.push(cnode);
        }*/else if(cnode.type != '#comment'){
            snode.push(cnode);
        }
    }
    //console.log(snode);
    node = snode; i = 0;
    for(len = snode.length; i < len; i++){
        cnode = snode[i];
        if(typeof cnode == 'string'){
            if(cnode.trim().length > 0){
                //console.log(cnode);
                hasrawstring = true;
                updates.push({idx:i,fix:0}); //0-self,1-head,2-body,3-tail
            }
        }else if(/span|i|cite|em|strong/i.test(cnode.type)){
            if(cnode.content && cnode.content.length == 1 && cnode.content[0].type == undefined && cnode.content[0].trim().length > 0){
                //console.log(cnode.content[0]);
                hasrawstring = true;
                updates.push({idx:i,fix:0});
            }
        }else if(cnode.type == 'br' && cnode.content && cnode.content.length == 1){
            if(cnode.content[0].type == undefined){
                hasrawstring = true;
                cnode.content[0] = '<'+tag+'>'+cnode.content[0]+'</'+tag+'>';
                //html = html.replace(cnode.content[0],'<'+tag+'>'+cnode.content[0]+'</'+tag+'>');
            }
        }
    }
    if(!hasrawstring){
        //console.log();
        elem.innerHTML = reformatJsonToHTML(snode,'',tag);
        //console.log(elem.innerHTML);
        return;
    }  

    i = 0;
    for(len = updates.length-1; i < len; i++){
        if( updates[i+1].idx == updates[i].idx+2 ){
            nnode = snode[updates[i].idx+1];
            //MIGHT NEED MORE TAGS
            if(/span|i|cite|em|strong/.test(nnode.type)){
                if(updates[i].fix == 3){
                    updates[i].fix = 2;
                    updates[i+1].fix = 3; 
                }else{
                    updates[i].fix   = 1;
                    updates[i+1].fix = 3; 
                }
            }
        }else if(updates[i+1].idx == updates[i].idx+1){
            if(updates[i].fix == 3){
                updates[i].fix   = 2;
                updates[i+1].fix = 3;
            }else{
                updates[i].fix = 1;
                updates[i+1].fix = 3;
            }
        }
    }
    i = 0;
    for(len = updates.length; i < len; i++){
        up = updates[i];
        cnode  = snode[up.idx];
        //console.log(cnode);
        if(typeof cnode == 'string'){
            if(up.fix == 0){
                snode[up.idx] = '<'+tag+'>'+cnode+'</'+tag+'>';
            }else if(up.fix == 1){
                snode[up.idx] = '<'+tag+'>'+cnode;
            }else if(up.fix == 3){
                snode[up.idx] = cnode+'</'+tag+'>';
            }
        }else{
            //console.log(node[up.idx]);
            snode[up.idx]['fix'] = updates[i].fix;
        }
    }
    //console.log(snode);
    //console.log(reformatJsonToHTML(node,'',tag));
    //if(hasrawstring) elem.innerHTML = html;
    //elem.innerHTML = reformatJsonToHTML(snode,'',tag);
    //console.log(updates,snode);
    $(elem).replaceWith('<span class="seed-fig-processed has-image">'+reformatJsonToHTML(snode,'',tag)+'</span>');
}

var findCaption = function(elem, tag) {
    var pare = elem.parent();
    if(elem[0].parentNode){
        if( pare.hasClass('seed-fig-processed') ) return;
        else pare.addClass('seed-fig-processed');
        wrapRawtext(elem[0].parentNode,tag);
    }
}
//remove tags with these classes
var removeExtra = function(){
    var removeClass = ['.CloseColumn','.OriginalLabel','.tc_mark','.app_promo','#check-it-out-prompt',
        '.postmetadata','.teaser_title','.teaser_date','div.timestamp','div.user-bylines','ul.author-list','.entry-eyebrow','.trending-badge','.buzz_source_links','#reactions-related','aside','.ad-mobile','.js_like_icon',
        '#author-box','.channel-label','.meta-avatar','.post-dropdown-ct','noscript','fb\\:like','g\\:plusone','font','.hot_stats_bar','.user-bylines',
        'section.contributions','.app-promo','.rectangleAd','.header-share','.nextPost','.fbshare','.hidden-xs',
        '.rweb-ad','.buttonHolder','.nav-main','.expand-marker--cta','.relatedPosts','.articleShare','.single-post-pagination',
        '#comments','#next-post-thumbnails','.m-carousel','.user-tier-box','.see-also','#categorypromo','.article-social-wrapper','.marketing-text-body-small',
        '.js-track-organizer-link','#fb-msg','div.related','label.field-label-hidden','div.metadata','.visually-hidden','.wsj-slideshow-fullscreen-handle','.image-enlarge','div.wsj-slideshow-counter',
        'div.wsj-slideshow-wrap','div.sponsored-post-list-wrapper','div.more-posts','div.viral-next-up','.photoslider_drag_message','div.fyre-widget','div.pagination-links',
        'div.facebook-promo','div.error_content','span.hidden','div.hidden','div.quiz_result_area','div.poll_preview_btns','div.poll_stats_wrapper','form','div#block-boxes-mental_floss_bottom_circ_banner','div.sidebar','div.fb-msg','div#boxes-box-article_addthis_top',
        'nav.nav-article','div.sgc__partner','div.buzz_superlist_item_link_buzz','img.spinner','.article-links','div#mobile_sign_up','section.mobile_pop_com','div.time-outer','.rail-ad','nav.container>ul','.loading','div#brand_bar',
        'div.corrections','cite.slideshow-link-js','div.popular-feed-title','div.more-from-feed','div.fact-hit-box','div#amazing-fact-left-wrapper','div#end-article-sharethrough','div#profile-cards','div#daily-events-newsletter',
        'div.load-more','div.landing-feed-container','div.ad-gpt-breaker','div.promo','div.js-postActionsBar',
        'div.aside-related-articles','div.article-footer','ul.nav-pills.menu','.load-next-post-btn','div#sticky-navbar','div.rub-button-overlay','div.l-sidebar','cite.next-link','div.js_foreplay','.read-more','div.recommended-articles-wrap','div.recommended-widget','section.recommended-list',
        'cite.vertical-label','em.vertical-label','header.adown','div.post-top-meta','section.global_sharing_controls','p.icon--svg','div.post-bottom','select','textarea','div#sidebar','div.list-comments','div#respond','div.bloc-related-post','div.related-posts',
        'h3#respond','ul.commentlist','.hide','div.sub-content-body','section.content-sharing','h3.article-header-topic','.pre-ko-custom-hide','div.js-article-actions','#floatingAd','.mobile-hide','.NS_projects__back_and_star','.js-reset-video-once','.forces-video-controls_hide',
        'div.ExternalVisitor','.SignupFormContainer','div.localInsights','div#signupForm','div.post-footer','ul.author_links','ul.channels_posted_to','.public_hide','section.post_meta','div.page_top_actions','div.slideshow-control','.gallery-button','p.boilerplate-label','div.post-boilerplate','cite.contribute','div.slideshow>.enter-wrapper','div.slideshow>.controls'
        ];
        //'div.slide-copy',
    for( var i = 0, ref = removeClass.length; i < ref; i++ ){
        var tag = removeClass[i];
        $(tag).each(function(){ $(this).remove() });
    }
}
//style all q and blockquotes
var fixQuote = function(){
    $('blockquote').each(function(){
        var elem = $(this);
        var pare = elem.parent();
        if(pare.is('p')){
            pare.replaceWith("<q>"+elem.html()+"</q>");
        }
    }).addClass('text-size');
    $('q').each(function(){
        var elem = $(this);
        var pare = elem.parent();
        if(pare.is('p')){
            pare.replaceWith("<q>"+elem.html()+"</q>");
        }
    }).addClass('text-size');
}
//save image from <a> which we gonna remove
var rescueImage = function(elem) {
    var pare   = elem.parent();
    var hasimg = false;
    elem.children().each(function() {
        if ($(this).is('img')) hasimg = true;
    });
    if( hasimg ) elem.replaceWith(elem.html());
}

var replaceTag = function(elem, newtag){
    var html = elem.html().trim();
    if( html.length == 0 ){ elem.remove(); return; }
    var css  = elem.attr('class');
    if(css != null)
        elem.replaceWith('<'+newtag+' class="'+css+'"> '+elem.html()+' </'+newtag+'>');
    else 
        elem.replaceWith('<'+newtag+'> '+elem.html()+' </'+newtag+'>');    
}

/*var delayToImg = function(elem){
    var src = elem.attr('data-imagesrc');
    if(src != undefined){
        src = src.replace('{width}',320);
        src = src.replace('{height}',480);
        elem.replaceWith('<img src="'+src+'" width="'+window.innerWidth+'" height="'+window.innerHeight+'">');
    }
}*/

var updateTag = function(elem, parentTags, newtag) {
    var pare = elem.parent();
    var css  = elem.attr('class');
    if ( pare ) {
        for( var i = 0, m = parentTags.length; i < m; i++ ){
            if(pare.is(parentTags[i])) {
                elem.replaceWith(elem.html());
                if(css != null) pare.addClass(css);
                return;
            }
        }
    }
    if(css != null)
        elem.replaceWith('<'+newtag+' class="'+css+'">'+elem.html()+'</'+newtag+'>');
    else 
        elem.replaceWith('<'+newtag+'>'+elem.html()+'</'+newtag+'>');    
}

var switchTag = function(element, newtag){
    var elem = $(element);
    var css  = elem.attr('class');
    if(css != null)
        elem.replaceWith('<'+newtag+' class="'+css+'">'+elem.html()+'</'+newtag+'>');
    else
        elem.replaceWith('<'+newtag+'>'+elem.html()+'</'+newtag+'>');            
}

var $;
module.exports = function(dom) {
    $ = dom;
    //remove extra stuff
    removeExtra();
    //fix quotes
    fixQuote();
    //fix recursive
    var recustivetags = ['text','span'];
    for( var i = 0, ref = recustivetags.length; i < ref; i++ ){
        var tag = recustivetags[i];
        $(tag).each(function(){ fixRecursive(this, tag); });
    }
    //update these tags with a styled one
    $('a').each(function(){
        var elem = $(this);
        rescueImage(elem);
        replaceTag(elem,'cite');
    });
    $('div,article').each(function(){
        var elem    = $(this);
        var classes = elem.attr('class'); //this.className;
        var id      = elem.attr('id'); //this.id;
        if(classes == undefined) return;
        if(id == undefined) id = '';
        if(/content|quote|m-snippet|tc_intro|field-item|cat-wrapper-top|kaikai|entry/i.test(classes) || /spoiler/i.test(id)){
            wrapRawtext(this,'p');
        }
    });
    $('text').each(function(){
        var elem = $(this);
        rescueImage(elem);
        updateTag(elem, ['p','h1','h2','h3','h4','h5','h6','blockquote','q','div.img_wrapper'],'p');
    });
    $('span').each(function(){
        var elem = $(this);
        rescueImage(elem);
    });
    $('p').each(function() {
        var elem = $(this);
        updateTag(elem, ['p','h1','h2','h3','h4','h5','h6','blockquote','q'],'p');
    });
    $("img").each(function() { 
        var elem = $(this);
        var pare = elem.parent();
        if(pare[0] != undefined){
            var node = pare[0].name;
            if(/P|H3|H1|H2|H4|H5|H6/i.test(node))
                pare.addClass('has-image');
        }
        findCaption(elem, 'small class="image-credits image description"');
    });
    $('iframe').each(function(){
        var elem = $(this);
        var pare = elem.parent();
        if(pare[0] != undefined){
            var node = pare[0].name;
            if(/P|H3|H1|H2|H4|H5|H6/i.test(node))
                pare.addClass('has-image');
        }
    });
    //$('span').each(function(){ updateTag($(this), ['p','h1','h2','h3','h4','h5','h6','blockquote','q'],'w'); });
};
