var TIMEOUT = 100;

/**
 *  日志
 *  trace 跟踪
 *  debug 调试
 *  info  信息
 *  warn  警告
 *  error 错误
 *  fatal 致命
 */
var log4js = require('log4js'); // Port of Log4js to work with node
var logger = log4js.getLogger('parse');
logger.setLevel('INFO');
var pmx = require('pmx'); // Keymetrics++ and PM2 adapter

/**
 *  This is a client for RabbitMQ (and maybe other servers?).
 *  It partially implements the 0.9.1 version of the AMQP protocol.
 */
var amqp = require('amqp'); // AMQP driver for node
var connection = amqp.createConnection({ url: process.env.MQ_URL });
connection.on('ready', function () { // Wait for connection to become established
  logger.info('Connection is ready');
});

var isJSON = require('is-json'); // check if a string is a valid JSON string without using Try/Catch
var geourl = require('geourl-lite'); // Get a link's countries' code

var Article = require('../../../model/mongodb/Article.js')(); // 文章实体

var header = { 'Content-Type': 'text/plain; charset=utf-8', 'Access-Control-Allow-Origin': '*' }; // 返回头部信息

var mayis = ['CN']; // 除美国(US)外的解析队列

/**
 *  解析操作
 */
var parseAct = function (req, res) {
  var force = req.query.force ? true : false;
  logger.trace('force:', force);
  var userId = req.query.userId;
  if (!userId) {
    userId = req.body.userId;
  }
  logger.trace('userId:', userId);
  var url = req.query.url;
  if (!url) {
    url = req.body.url;
  }
  if (url) {
    return parseUrl(res, url, userId, force, true); // 解析 URL
  }
  var selector = req.query.selector;
  if (!selector) {
    selector = req.body.selector;
  }
  logger.trace('selector:', selector);
  if (!selector || !isJSON(selector)) {
    res.send('no query');
    return;
  }
  selector = JSON.parse(selector);
  Article.find(selector, 'link', { limit: 1000 }, function (err, docs) {
    if (err) {
      pmx.notify({ event: 'find article', error: 'find articles error:' + err });
      logger.fatal('find articles error:', err);
      res.sendStatus(500);
      return;
    }
    if (!docs) {
      pmx.notify({ event: 'find article', error: 'find articles empty' });
      logger.fatal('find articles empty');
      res.sendStatus(404);
      return;
    }
    logger.info('find ' + docs.length + ' articles');
    logger.debug('articles\' data:', docs);
    docs.forEach(function (item) {
      logger.debug('item:', item);
      parseUrl(res, item.link, userId, force); // 解析 URL
    });
    res.writeHead(200, header);
    res.end('success');
  });
};

/**
 *  解析 URL
 */
var parseUrl = function (res, url, userId, force, once) {
  var start = new Date();
  logger.trace('url:', url);
  geourl(url, function (err, geo) {
    if (err) {
      pmx.notify({ event: 'geo url', error: 'geo url[' + url + '] error:' + err });
      logger.fatal('geo url[%s] error:', url, err);
      if (once && err.code != 'ESERVFAIL') {
        res.send(err.message);
        return;
      }
    }
    var countries = geo.countries;
    logger.debug('%s \'s countries:', url, countries);
    var queueName = getQueueByCountries(countries);
    connection.queue(queueName, function (queue) {
      geo.force = force;
      geo.userId = userId;
      connection.publish(queueName, { geo: geo, start: new Date() });
      logger.debug('publish url[%s] by %s to queue:', url, userId, queueName);
    });
    if (once) {
      res.writeHead(200, header);
      res.end('success');
    }
    var millis = new Date() - start;
    logger.info('parse url[%s] success:', url, millis);
    if (millis > TIMEOUT) {
      pmx.notify({ event: 'helloseed api timeout', error: 'parse url[' +  url + '] timeout:' + millis });
    }
  });
};

/**
 *  根据国家编码获取处理队列
 */
var getQueueByCountries = function (countries) {
  var queueName = 'us';
  mayis.forEach(function (mayi, index) {
    if (countries.indexOf(mayi) != -1) {
      queueName = mayi.toLowerCase();
    }
  });
  return queueName + '-queue';
};

module.exports = parseAct;