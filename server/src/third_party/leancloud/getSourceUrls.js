var request = require('request');

var url = 'https://api.leancloud.cn/1.1/classes/Site';

var options = {
  headers: {
    'Content-Type': 'application/json',
    'X-AVOSCloud-Application-Id': 'm7u3qeqxmoxu2xxn3ainynms7ty0z01hl5kkt7tdn4c9lngl',
    'X-AVOSCloud-Application-Key': 'fk32q34idm2iwmvs1qewa6ynoquousjmr0xoeepdoczrrtgv'
  }
};

module.exports = function (callback) {
  request(url, options, function (err, resp, body) {
    if (err) {
      return callback(err);
    }
    if (resp.statusCode != 200) {
      return callback(new Error('status code: ' + resp.statusCode));
    }
    var sourceUrls = {};
    var results = JSON.parse(body).results;
    results.forEach(function (result) {
      var type = result.typeName;
      if(!sourceUrls[type]) {
        sourceUrls[type] = [];
      }
      sourceUrls[type].push({ name: result.name, url: result.url });
    });
    return callback(null, sourceUrls);
  });
};