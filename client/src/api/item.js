var TIMEOUT = 100; // the api timeout for the item (in milliseconds).

/**
 *  日志
 *  trace 跟踪
 *  debug 调试
 *  info  信息
 *  warn  警告
 *  error 错误
 *  fatal 致命
 */
var log4js = require('log4js'); // Port of Log4js to work with node
var logger = log4js.getLogger('item');
logger.setLevel('INFO');
var pmx = require('pmx'); // Keymetrics++ and PM2 adapter

var Article = require('../../../model/mongodb/Article.js')(); // 文章实体
var projectionDefault = require('../common/constants.js').projectionDefault; // 默认返回字段

var header = { 'Content-Type': 'text/plain; charset=utf-8', 'Access-Control-Allow-Origin': '*' }; // 返回头部信息

/**
 *  获取 article 接口
 */
var itemApi = function (req, res) {
  var start = new Date();
  var selector = {};
  var id = req.query.id; // 文章 id
  if (!id) {
    id = req.body.id;
  }
  logger.trace('id:', id);
  if (id) {
    selector._id = id;
  } else {
    var url = req.query.url;
    if (!url) {
      url = req.body.url;
    }
    logger.trace('url:', url);
    selector.link = url;
  }
  var projection = req.query.projection; // 返回字段
  if (!projection) {
    projection = projectionDefault;
  } else if (projection == 'all') {
    projection = '';
  }
  Article.findOne(selector, projection, function (err, doc) {
    if (err) {
      pmx.notify({ event: 'find article', error: 'find article[' + id + '/' + url + '] error:' + err });
      logger.fatal('find article[%s/%s] error:', id, url, err);
      res.sendStatus(500);
      return;
    }
    if (!doc) {
      pmx.notify({ event: 'find article', error: 'find article[' + id + '/' + url + '] empty' });
      logger.fatal('find article[%s/%s] empty', id, url);
      res.sendStatus(404);
      return;
    }
    if (doc.linkDomain == 'nautil') {
      doc.linkDomain += '.' + doc.linkTld;
    }
    logger.info('article:', doc);
    res.writeHead(200, header);
    res.end(JSON.stringify(doc));
    var millis = new Date() - start;
    logger.info('get item [%s/%s] time:', id, url, millis);
    if (millis > TIMEOUT) {
      pmx.notify({ event: 'helloseed api timeout', error: 'get item [' +  id + '/' + url + '] timeout:' + millis });
    }
    return;
  });
};

module.exports = itemApi;